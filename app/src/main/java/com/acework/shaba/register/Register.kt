package com.acework.shaba.register

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import com.acework.shaba.MainActivity
import com.acework.shaba.R
import com.acework.shaba.home.HomeActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.HashMap

class Register : AppCompatActivity() {
    private var register: AppCompatButton? = null
    private var login: TextView? = null
    private var name: EditText? =null
    private var email: EditText? = null
    private  var mobile: EditText? = null
    private var members: EditText?=null
    private  var password: EditText? =null
    private var confirmpassword: EditText? = null
    private var loadingBar: ProgressDialog? = null
    private var radioGroup: RadioGroup?=null
    private var radioButton: RadioButton?=null
    private var category_radio: String?=null

    private lateinit var auth: FirebaseAuth
    private lateinit var authphone:FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        register=findViewById<AppCompatButton>(R.id.register)
        login=findViewById<TextView>(R.id.loginsign)
        name=findViewById<EditText>(R.id.usersign)
        email=findViewById<EditText>(R.id.emailsign)
        mobile=findViewById<EditText>(R.id.phonesign)
        members=findViewById<EditText>(R.id.noofmembers)
        password=findViewById<EditText>(R.id.passwordsign)
        confirmpassword=findViewById<EditText>(R.id.confirmpasswordsign)
        loadingBar = ProgressDialog(this)
        radioGroup = findViewById<RadioGroup>(R.id.radio_Categories);

        auth = FirebaseAuth.getInstance()
        authphone= FirebaseAuth.getInstance()


        register!!.setOnClickListener(View.OnClickListener {
            Register()


        })

        login!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@Register, MainActivity::class.java)
          startActivity(intent)
           finish()
        })

    }

    private fun Register(){
        val selectedId = radioGroup!!.checkedRadioButtonId

        radioButton = findViewById(selectedId) as RadioButton

        category_radio= radioButton!!.text.toString();

        val name: String = name?.text.toString()
        val email: String=email?.text.toString()
        val phone: String = mobile?.text.toString()
        val memberss: String=members?.text.toString()
        val password: String = password?.text.toString()
        val confirm: String =confirmpassword?.text.toString()

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Please write your name...", Toast.LENGTH_SHORT).show()
        }
//        else if(TextUtils.isEmpty(email)) {
//            Toast.makeText(this, "Please write your email...", Toast.LENGTH_SHORT).show()
//        }
        else if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "Please write your phone number...", Toast.LENGTH_SHORT).show()
        }
        else if (TextUtils.isEmpty(memberss)) {
            Toast.makeText(this, "Please write the number of members...", Toast.LENGTH_SHORT).show()
        }
        else if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please write your password...", Toast.LENGTH_SHORT).show()
        }
        else if(!(password.equals(confirm))){
            Toast.makeText(this, "Please confirm password match...", Toast.LENGTH_SHORT).show()
        }
        else{
            loadingBar?.setTitle("Create Account")
            loadingBar?.setMessage("Please wait, while we are checking the credentials.")
            loadingBar?.setCanceledOnTouchOutside(false)
            val show = loadingBar?.show()
            ValidatePhoneNumber(name, email,memberss, phone, password, category_radio!!)
        }
    }

    private fun ValidatePhoneNumber(
        name: String,
        email: String,
        memberss:String,
        phone: String,
        password: String,
        category_radio: String
    )
    {
//        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener{ task ->
//            if(task.isSuccessful){
////                Toast.makeText(this, "Successfully Registered", Toast.LENGTH_LONG).show()
////                val intent = Intent(this, MainActivity::class.java)
////                startActivity(intent)
////                finish()
//            }else {
//                Toast.makeText(this, "Registration Failed", Toast.LENGTH_LONG).show()
//            }
//        })
//

        val RootRef= FirebaseDatabase.getInstance().reference;
        RootRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {


//                if(snapshot.child("Users").hasChild(phone)){
//                    Toast.makeText(this@Register, "This $phone already exists.", Toast.LENGTH_SHORT).show()
//                    loadingBar!!.dismiss()
//                    Toast.makeText(this@Register, "Please try again using another phone number.", Toast.LENGTH_SHORT).show()
//                    val intent = Intent(this@Register, Register::class.java)
//                    startActivity(intent)
//
//                } else {


                    val userdataMap = HashMap<String, Any>()
                    userdataMap["phone"] = phone
                    userdataMap["password"] = password
                    userdataMap["name"] = name
                    userdataMap["email"]=email
                    userdataMap["members"]=memberss
                    userdataMap["category"]=category_radio

                    RootRef.child("Users").child(phone).updateChildren(userdataMap)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(this@Register, "Congratulations, on your first step to create an account.", Toast.LENGTH_SHORT).show()
                                loadingBar!!.dismiss()
                                val intent = Intent(this@Register, PhoneOTP::class.java)
                                intent.putExtra("phoneNo",phone)
                                startActivity(intent)
                                finish()
                            } else {
                                loadingBar!!.dismiss()
                                Toast.makeText(this@Register, "Network Error: Please try again after some time...", Toast.LENGTH_SHORT).show()
//                                Toast.makeText(this@Register, "This $phone already exists.", Toast.LENGTH_SHORT).show()
//                                loadingBar!!.dismiss()
//                                Toast.makeText(this@Register, "Please try again using another phone number.", Toast.LENGTH_SHORT).show()
                            }
                        }
               // }
            }


            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser != null){
            val intent= Intent(this, HomeActivity::class.java )
            intent.putExtra("user",currentUser)
            startActivity(intent)
        }
    }
}