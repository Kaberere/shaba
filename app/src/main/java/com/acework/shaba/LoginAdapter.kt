package com.acework.shaba

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class LoginAdapter internal constructor(myContext: Context, fm: FragmentManager, var totalTabs: Int) : FragmentStatePagerAdapter(fm){
    override fun getCount(): Int {
       return totalTabs
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return LoginTabFragment()
            }
            1 -> {
                return LoginAdminFragment()
            }
    }
        throw IllegalStateException("position $position is invalid for this viewpager")

}
    }