package com.acework.shaba.ui.home.orders

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.ui.home.HomeActivityTwo
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.google.firebase.database.*
import java.util.ArrayList

class Orders : AppCompatActivity() {
    private var image: ImageView? = null
    private var dimensions: TextView?= null
    private var color: TextView?= null
    private var price: TextView? =null
    private var quantity: TextView? =null
    private var cartRef: DatabaseReference? = null
    private var orderno: String? =null
    private var oid: String?=null
    private var key: String?=null
    private var category: String?=null
    private var description: TextView?=null
    private var imageSlider: ImageSlider?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders)

        oid = intent.getStringExtra("oid")
        key=intent.getStringExtra("key")
        category=intent.getStringExtra("category")

     //   val order =intent.getParcelableExtra<OrderModel>("order")

        orderno = intent.getStringExtra("orderno")

      //  image = findViewById<ImageView>(R.id.image_order_single)
        dimensions=findViewById<TextView>(R.id.single_dimension)
        color = findViewById<TextView>(R.id.single_color)
        description=findViewById<TextView>(R.id.single_description)
        price= findViewById<TextView>(R.id.single_price)
        quantity=findViewById<TextView>(R.id.single_quantity)
        imageSlider=findViewById(R.id.image_slider_artisan)

      //  val goback: AppCompatButton=findViewById(R.id.goback)

        if(category.equals("Weaver")) {
            cartRef = FirebaseDatabase.getInstance().reference.child("CheckOut")
        }
        else if(category.equals("Wood work")){
            cartRef = FirebaseDatabase.getInstance().reference.child("CheckOutWood")
        }
       // cartRef= FirebaseDatabase.getInstance().reference.child("CheckOut")
        getDetails(oid,key)

      //  Picasso.get().load((order!!.image).toString()).into(image)
//        order!!.image?.let { image!!.setImageResource(it) }
//        dimensions!!.text = (order!!.dimensions).toString()
//        color!!.text =(order.color).toString()
//        price!!.text=(order.price).toString()
//        quantity!!.text=(order.quantity).toString()

//        goback.setOnClickListener(View.OnClickListener {
//            val intent = Intent(this, HomeActivityTwo::class.java)
//            intent.putExtra("cid",key)
//            startActivity(intent)
//            finish()
//        })
    }

    private fun getDetails(oid: String?, key: String?) {

        if(oid!=null){
            cartRef!!.child(key!!).child("Order").child(oid).addValueEventListener(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                 if(snapshot.exists()){
                     var cartModel: CartModel?=snapshot.getValue(CartModel::class.java)
                     dimensions!!.text=cartModel!!.dimensions
                     price!!.text=cartModel.price
                     quantity!!.text=cartModel.quantity
                     color!!.text=cartModel.color
                     description!!.text=cartModel.description
                     //Glide.with(this@Orders).load(cartModel.image).into(image!!)



                 }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            })
        }

    }


}