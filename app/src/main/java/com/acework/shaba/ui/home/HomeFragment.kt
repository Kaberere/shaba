package com.acework.shaba.home.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.admin.orders.cart.CheckOutModel
import com.acework.shaba.ui.home.OrderRecyclerAdapter
import com.acework.shaba.ui.home.OrderViewHolderTwoT
import com.acework.shaba.ui.home.orders.Orders
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*


class HomeFragment : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar? = null

    private var orderAdapter: OrderRecyclerAdapter?= null
    private var images: IntArray? =null;
    private var cartRef: DatabaseReference? = null
    private var orderRef: DatabaseReference?=null
    private var keysT: String?=null
    private var cid: String?=null
    //private val orderList = generateDummyList(20)

    //Home screen
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view=inflater.inflate(R.layout.fragment_home, container, false)
        progressBar = view.findViewById<ProgressBar>(R.id.progress_bar_orders)

        recyclerView=view.findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        recyclerView!!.setLayoutManager(layoutManager)

//        orderAdapter = OrderRecyclerAdapter(orderList,this)
//
//
//        recyclerView!!.adapter=orderAdapter

        cid= activity?.intent!!.getStringExtra("cid")
        progressBar!!.visibility = View.GONE
        cartRef= FirebaseDatabase.getInstance().reference.child("CheckOut")
//getData(cartRef!!)


        orderRef=cartRef!!.child(cid!!).child("Order")

        bindView(orderRef)



        return view
    }

//    private fun generateDummyList(size: Int) : ArrayList<OrderModel> {
//        val list = ArrayList<OrderModel>()
//
//        for(i in 0 until size){
//            val drawable = when(i % 20) {
//                0 -> R.drawable.basket
//                1 -> R.drawable.basket10
//                2 -> R.drawable.basket2
//                3 -> R.drawable.basket3
//                4 -> R.drawable.basket5
//                5-> R.drawable.basket6
//                6 -> R.drawable.basket7
//                7 -> R.drawable.basket8
//                8 -> R.drawable.basket9
//                9 -> R.drawable.basketa
//                10 -> R.drawable.basketb
//                11 -> R.drawable.basketc
//                12 -> R.drawable.basketd
//                13 -> R.drawable.baskete
//                14 -> R.drawable.basketf
//                15-> R.drawable.basketg
//                16-> R.drawable.basketh
//                17-> R.drawable.basketi
//                18-> R.drawable.basketk
//
//
//                else -> R.drawable.basketl
//
//            }
//            val item = OrderModel("18/09/2021","order $i" ," 3 x 2","yellow",drawable,"10","shs 1000" )
//            list += item
//        }
//
//        return list
//    }

//    override fun onStart() {
//        super.onStart()
//
//
//        cartRef!!.addValueEventListener(object : ValueEventListener{
//            override fun onDataChange(snapshot: DataSnapshot) {
////
//                if (snapshot.exists()) {
//
//                    var checkOutModel: CheckOutModel? = snapshot.getValue(CheckOutModel::class.java)
//
//                    key=checkOutModel!!.cid
//                    orderRef = cartRef!!.child(key!!).child("Order")
//                    var options = FirebaseRecyclerOptions.Builder<CartModel>()
//                        .setQuery(orderRef!!, CartModel::class.java)
//                        .build()
//
//                    var adapter: FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT> = object :
//                        FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT>(options) {
//                        override fun onCreateViewHolder(
//                            parent: ViewGroup,
//                            viewType: Int
//                        ): OrderViewHolderTwoT {
//                            val view: View = LayoutInflater.from(parent.context)
//                                .inflate(R.layout.orders_layout, parent, false)
//                            return OrderViewHolderTwoT(view)
//                        }
//
//                        override fun onBindViewHolder(
//                            orderViewHolderTwoT: OrderViewHolderTwoT,
//                            p1: Int,
//                            cartModel: CartModel
//                        ) {
//                            val oid = cartModel.oid
//
//                            orderViewHolderTwoT.txtOrderName.text = cartModel.color
//                            orderViewHolderTwoT.txtDimensions.text = cartModel.dimensions
//                            orderViewHolderTwoT.txtDate.text = cartModel.oid
//
//                            Glide.with(activity!!).load(cartModel.image)
//                                .into(orderViewHolderTwoT.imageView)
//
//                            orderViewHolderTwoT.itemView.setOnClickListener(View.OnClickListener {
//                                val intent = Intent(activity, Orders::class.java)
//                                intent.putExtra("oid", oid)
//                                startActivity(intent)
//                            })
//                        }
//
//                    }
//
//                    recyclerView!!.adapter = adapter
//                    adapter.startListening()
//
//                    // orderRef = cartRef!!.child(key!!).child("Order")
//
//
//                }
//            }
//
//
//            override fun onCancelled(error: DatabaseError) {
//                TODO("Not yet implemented")
//            }
//
//        })
//
//
//    }

   private fun getData(cartRef: DatabaseReference) {

       cartRef.addValueEventListener(object : ValueEventListener{
           override fun onDataChange(snapshot: DataSnapshot) {
               for (childSnapshot: DataSnapshot in snapshot.children){
                   keysT=childSnapshot.key
                   Log.d("KEY",keysT!!)

                   cartRef.child(keysT!!).addValueEventListener(object : ValueEventListener{
                       override fun onDataChange(snapshot: DataSnapshot) {
                         if(snapshot.exists()){
                             var checkOutModel: CheckOutModel? = snapshot.getValue(CheckOutModel::class.java)
                            var key: String= checkOutModel!!.cid.toString()

                             orderRef = cartRef!!.child(key!!).child("Order")

                           //  bindView(orderRef!!,key!!)



                    // orderRef = cartRef!!.child(key!!).child("Order")




                         }
                       }

                       override fun onCancelled(error: DatabaseError) {

                       }

                   })
               }
           }

           override fun onCancelled(error: DatabaseError) {

           }


       })
   }

    private fun bindView(orderRef: DatabaseReference?) {


        var options = FirebaseRecyclerOptions.Builder<CartModel>()
            .setQuery(orderRef!!, CartModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT> = object :
            FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT>(options) {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): OrderViewHolderTwoT {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.orders_layout, parent, false)
                return OrderViewHolderTwoT(view)
            }

            override fun onBindViewHolder(
                orderViewHolderTwoT: OrderViewHolderTwoT,
                p1: Int,
                cartModel: CartModel
            ) {
                val oid = cartModel.oid

                orderViewHolderTwoT.txtOrderName.text = cartModel.color
                orderViewHolderTwoT.txtDimensions.text = cartModel.dimensions
                orderViewHolderTwoT.txtDate.text = cartModel.oid

                Glide.with(activity!!).load(cartModel.image)
                    .into(orderViewHolderTwoT.imageView)

                orderViewHolderTwoT.itemView.setOnClickListener(View.OnClickListener {
                    val intent = Intent(activity, Orders::class.java)
                    intent.putExtra("oid", oid)
                    intent.putExtra("key",cid)
                    startActivity(intent)
                })
            }

        }

        recyclerView!!.adapter = adapter
        adapter.startListening()
    }
    }
