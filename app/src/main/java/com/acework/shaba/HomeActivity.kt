 package com.acework.shaba.home

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.viewpager.widget.ViewPager
import com.acework.shaba.MainActivity
import com.acework.shaba.R
//import com.acework.shaba.databinding.ActivityHomeBinding
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase



 class HomeActivity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    //private lateinit var binding: ActivityHomeBinding
    private lateinit var auth: FirebaseAuth
    private var user: String?=null
    private var mAuthListener: AuthStateListener? = null
    private var viewPager: ViewPager?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)

        // Initialize Firebase Auth
       auth = Firebase.auth


        mAuthListener = AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                println("User logged in")
                val phone=user.phoneNumber
              //  Toast.makeText(this,"Online $phone", Toast.LENGTH_SHORT).show()
            } else {
                println("User not logged in")
                startActivity(Intent(this,MainActivity::class.java))
                finish()
            }
        }

        user= intent.getStringExtra("auth")


        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar_home)
        setSupportActionBar(toolbar)

        supportActionBar?.title = "Orders"

        var tabLayout: TabLayout? = findViewById<TabLayout>(R.id.tabLayout)

        tabLayout!!.addTab(tabLayout.newTab().setText("Orders"))

        tabLayout.addTab(tabLayout.newTab().setText("Payments"))


        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.tabTextColors = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"))


        viewPager = findViewById<ViewPager>(R.id.pager)

        val pagerAdapterHome = PageAdapterHome(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = pagerAdapterHome
        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })


        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout);

        val navigationView = findViewById<NavigationView>(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
       // menuInflater.inflate(R.menu.search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        /*  if (id == R.id.action_settings) {
            return true;
        }
*/
//        if (id == R.id.action_search) {
//            /*val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:0722201368"))
//            startActivity(intent)*/
//        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val id: Int = item.getItemId()
        var fragment: Fragment?=null
        if (id == R.id.nav_home_user) {
            val intent = Intent(this@HomeActivity, HomeActivity::class.java)
            startActivity(intent)
        }
        else if(id == R.id.nav_payment_user){
            viewPager!!.currentItem=1

        }

        else if(id==R.id.nav_logout_user){
            AuthUI.getInstance().signOut(this).addOnCompleteListener(OnCompleteListener {
                val intent = Intent(this@HomeActivity, MainActivity::class.java)
               FirebaseAuth.getInstance().signOut();
                startActivity(intent)
            })

        }
        return true

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        auth.addAuthStateListener(mAuthListener!!);
    }
}
