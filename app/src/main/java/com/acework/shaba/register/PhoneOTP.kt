package com.acework.shaba.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.acework.shaba.R
import com.acework.shaba.home.HomeActivity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class PhoneOTP : AppCompatActivity() {
    private var codeEt: EditText?=null
    private lateinit var progressBar: ProgressBar
    private var forceResendingToken: PhoneAuthProvider.ForceResendingToken?=null
    private var mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks?=null
    private var mVerificationId: String?=null
    private lateinit var firebaseAuth: FirebaseAuth
    private val TAG="PHONE_OTP"
    private var resendCodeTv: TextView?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_otp)

        codeEt=findViewById(R.id.otpedittext)
        progressBar=findViewById(R.id.progressbarOtp)
        resendCodeTv=findViewById(R.id.resendCodeTv)
        val submit: AppCompatButton=findViewById(R.id.submitOtp);

        val phone: String=intent.getStringExtra("phoneNo").toString()
        val category: String=intent.getStringExtra("category").toString()

        firebaseAuth= FirebaseAuth.getInstance()

        mCallbacks=object: PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {

              //  signInWithPhoneAuthCredential(phoneAuthCredential)

            }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(this@PhoneOTP,"${e.message}", Toast.LENGTH_SHORT).show()

            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
               Log.d(TAG,"onCodeSent: $verificationId")
                mVerificationId=verificationId
                forceResendingToken=token
            }

        }



        startPhoneNumberVerification(phone)

        resendCodeTv!!.setOnClickListener(View.OnClickListener {


        })

        submit.setOnClickListener(View.OnClickListener {
            val code=codeEt!!.text.toString().trim()
            if(TextUtils.isEmpty(code)){
                Toast.makeText(this@PhoneOTP,"Please enter verification code", Toast.LENGTH_SHORT).show()
            }
            else{
                verifyPhoneNumberWithCode(mVerificationId,code,category)
            }
        })

        //sendVerificationToUser(phone)

    }

    private fun startPhoneNumberVerification(phone: String)
    {
        val options=PhoneAuthOptions.newBuilder(firebaseAuth)
            .setPhoneNumber(phone)
            .setTimeout(60L,TimeUnit.SECONDS)
            .setActivity(this)
            .setCallbacks(mCallbacks!!)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }

    private fun resendVerificationCode(phone: String,token: PhoneAuthProvider.ForceResendingToken?){
        val options=PhoneAuthOptions.newBuilder(firebaseAuth)
            .setPhoneNumber(phone)
            .setTimeout(60L,TimeUnit.SECONDS)
            .setActivity(this)
            .setCallbacks(mCallbacks!!)
            .setForceResendingToken(token!!)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private fun verifyPhoneNumberWithCode(verificationId: String?, code: String, category: String){
        val credential = PhoneAuthProvider.getCredential(verificationId!!,code)
        signInWithPhoneAuthCredential(credential,category)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential, category: String) {
        firebaseAuth.signInWithCredential(credential)
            .addOnSuccessListener {

                val phone = firebaseAuth.currentUser!!.phoneNumber
                Toast.makeText(this,"Logged in as $phone", Toast.LENGTH_SHORT).show()
                val intent = Intent(this@PhoneOTP, HomeActivity::class.java)
                startActivity(intent)
                finish()

            }
            .addOnFailureListener{e->
                Toast.makeText(this,"${e.message}", Toast.LENGTH_SHORT).show()
            }

    }
}