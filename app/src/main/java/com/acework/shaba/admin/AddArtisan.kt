package com.acework.shaba.admin

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.acework.shaba.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.text.SimpleDateFormat
import java.util.*

class AddArtisan : AppCompatActivity() {
    private var select_image: ImageView?=null
    private var category: EditText?= null
    private var GalleryPick: Int=1
    private var imageUri: Uri?=null

    private var categorys: String?=null
    private val loadingBar: ProgressDialog? = null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null
    private var artisanRandomKey: String? = null
    private  var downloadImageUrl:kotlin.String? = null
    private var artisanImagesRef: StorageReference? = null
    private var artisanRef: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_artisan)

        select_image=findViewById<ImageView>(R.id.select_Artisan_image)
        category=findViewById<EditText>(R.id.editTextArtisanCategory)


        val addCategory=findViewById<AppCompatButton>(R.id.addArtisan)

        artisanImagesRef = FirebaseStorage.getInstance().reference.child("Artisan Images")
        artisanRef = FirebaseDatabase.getInstance().reference.child("Artisans")



        select_image?.setOnClickListener(){
            OpenGallery()
        }
        addCategory.setOnClickListener(){
            ValidateOrderData()
        }

    }



    private fun OpenGallery() {
        val galleryIntent= Intent()
        galleryIntent.action = Intent.ACTION_GET_CONTENT
        galleryIntent.type = "image/*"

        startActivityForResult(galleryIntent, GalleryPick)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GalleryPick && resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.data
            select_image?.setImageURI(imageUri)
        }
    }
    private fun ValidateOrderData() {

        categorys=category?.text.toString()

        if (imageUri == null) {
            Toast.makeText(this, "Category image is mandatory...", Toast.LENGTH_SHORT).show()
        }else if (TextUtils.isEmpty(categorys)) {
            Toast.makeText(this, "Please write the artisan category...", Toast.LENGTH_SHORT).show()
        }
        else{
            StoreOrderInformation()
        }
    }

    private fun StoreOrderInformation() {
        loadingBar?.setTitle("Add New Artisan Category")
        loadingBar?.setMessage("Dear Admin, please wait while we are adding the new artisan.")
        loadingBar?.setCanceledOnTouchOutside(false)
        loadingBar?.show()

        val calendar = Calendar.getInstance()

        val currentDate = SimpleDateFormat("MMM dd, yyyy")
        saveCurrentDate = currentDate.format(calendar.time)

        val currentTime = SimpleDateFormat("HH:mm:ss a")
        saveCurrentTime = currentTime.format(calendar.time)

        artisanRandomKey = saveCurrentDate + saveCurrentTime
        val filePath: StorageReference = artisanImagesRef!!.child(imageUri!!.lastPathSegment + artisanRandomKey + ".jpg")

        val uploadTask = filePath.putFile(imageUri!!)
        uploadTask.addOnFailureListener { e ->
            val message = e.toString()
            Toast.makeText(this, "Error: $message", Toast.LENGTH_SHORT).show()
            loadingBar?.dismiss()
        }.addOnSuccessListener {
            Toast.makeText(this, "Artisan Image uploaded Successfully...", Toast.LENGTH_SHORT).show()
            val urlTask = uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                downloadImageUrl = filePath.downloadUrl.toString()
                filePath.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    downloadImageUrl = task.result.toString()
                    Toast.makeText(
                        this,
                        "got the event image Url Successfully...",
                        Toast.LENGTH_SHORT
                    ).show()
                    SaveOrderInfoToDatabase()
                }
            }
        }
    }

    private fun SaveOrderInfoToDatabase() {
        val artisanMap=HashMap<String, Any>()
        artisanMap["aid"]=artisanRandomKey!!
        artisanMap["category"]=categorys!!
        artisanMap["image"]=downloadImageUrl!!

        artisanRef?.child(artisanRandomKey!!)?.updateChildren(artisanMap)
            ?.addOnCompleteListener(OnCompleteListener<Void?> { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, AdminHome::class.java)
                    startActivity(intent)
                    loadingBar!!.dismiss()
                    Toast.makeText(
                        this,
                        "Artisan is added successfully..",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    loadingBar!!.dismiss()
                    val message = task.exception.toString()
                    Toast.makeText(this, "Error: $message", Toast.LENGTH_SHORT).show()
                }
            })
    }
}