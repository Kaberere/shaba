package com.acework.shaba.home.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.admin.payment.PaymentViewHolder
import com.acework.shaba.model.Users
import com.acework.shaba.ui.payments.PaymentModel
import com.acework.shaba.ui.payments.PaymentRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase


class Payments : Fragment() {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar? = null
    private var paymentRef: DatabaseReference? = null
    private var paymentRefWood: DatabaseReference? = null
    private lateinit var auth: FirebaseAuth
    private var userRef: DatabaseReference? = null

    private var paymentAdapter: PaymentRecyclerAdapter?= null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_home, container, false)
        progressBar = view.findViewById<ProgressBar>(R.id.progress_bar_orders)


        recyclerView=view.findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        recyclerView!!.layoutManager = layoutManager

        auth = Firebase.auth

        val textgone: TextView=view.findViewById(R.id.textgone)

        val user = auth.currentUser
        if (user != null) {
            val phone=user.phoneNumber
            userRef= FirebaseDatabase.getInstance().reference.child("Users").child(phone!!)
            println("User logged in")

            userRef!!.addListenerForSingleValueEvent (object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    var users: Users?=snapshot.getValue(Users::class.java)
                    var category: String?=users!!.category

                    if(category!!.equals("Weaver")){

                        paymentRef = FirebaseDatabase.getInstance().reference.child("Payments")
                        bindView(paymentRef!!)
                        textgone.visibility=View.INVISIBLE
                    }
                    else if(category!!.equals("Wood work")){
                        paymentRef= FirebaseDatabase.getInstance().reference.child("PaymentsWood")
                        bindView(paymentRef!!)
                        textgone.visibility=View.INVISIBLE
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(activity,"${error.message}", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            println("User not logged in")
        }


        return view
    }

    private fun bindView(paymentRef: DatabaseReference) {
        var options= FirebaseRecyclerOptions.Builder<PaymentModel>()
            .setQuery(paymentRef!!, PaymentModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<PaymentModel, PaymentViewHolder> = object :
            FirebaseRecyclerAdapter<PaymentModel, PaymentViewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.payment_layout, parent, false)
                return PaymentViewHolder(view)
            }

            override fun onBindViewHolder(paymentViewHolder: PaymentViewHolder, p1: Int, paymentModel: PaymentModel) {
                val pid = paymentModel.pid

                paymentViewHolder.txtMpesaCode.text=paymentModel.mpesacode
                paymentViewHolder.txtNumber.text=paymentModel.number
                paymentViewHolder.txtDate.text=paymentModel.date
                paymentViewHolder.txtAmount.text=paymentModel.amount

                progressBar!!.visibility = ProgressBar.INVISIBLE
            }

        }

        recyclerView!!.adapter=adapter
        adapter.startListening()
    }
    }

