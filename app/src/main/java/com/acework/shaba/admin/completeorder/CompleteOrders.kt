package com.acework.shaba.admin.completeorder

import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.acework.shaba.R
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.DatabaseReference

class CompleteOrders : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var completeRef: DatabaseReference? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_orders)

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbartCompleteOrder)
        setSupportActionBar(toolbar)

        supportActionBar?.title = "Complete Order"

        var tabLayout: TabLayout? = findViewById<TabLayout>(R.id.tabLayoutCompleteOrder)
        tabLayout!!.addTab(tabLayout.newTab().setText("Weaver"))

        tabLayout.addTab(tabLayout.newTab().setText("Woodwork"))

        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.tabTextColors = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"))

        val viewPager: ViewPager = findViewById<ViewPager>(R.id.pagerCompletePayment)
        val pagerAdapterComplete = PageAdapterComplete(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = pagerAdapterComplete
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }
}