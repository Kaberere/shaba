package com.acework.shaba.admin.model

data class ArtisanModel
    (var aid: String?,
     var category: String?,
     var image: String?
) {
        constructor(): this("","","")

}