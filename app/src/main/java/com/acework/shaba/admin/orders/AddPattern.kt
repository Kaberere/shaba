package com.acework.shaba.admin.orders

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.acework.shaba.R
import com.acework.shaba.admin.AdminHome
import com.acework.shaba.admin.model.ArtisanModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.text.SimpleDateFormat
import java.util.*


class AddPattern : AppCompatActivity() {
    private var numberOrder: Int?=1
    private var numberOrderTxt: TextView?=null
    private var dimensions: EditText?=null
    private var color: EditText?=null
    private var price: EditText?=null
    private var description: EditText?=null
    private var selectImage: ImageView?=null
    private var GalleryPick: Int=1
    private var imageUri: Uri?=null
    private var dimensioninput: String?=null
    private var colorinput: String?=null
    private var priceinput: String?=null
    private var descriptioninput: String?=null
    private var loadingBar: ProgressDialog? = null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null
    private var orderRandomKey: String? = null
    private var orderImagesRef: StorageReference? = null
    private  var downloadImageUrl:kotlin.String? = null
    private var orderRef: DatabaseReference? = null
    private var orderRefTwo: DatabaseReference? = null
    private var aid: String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_order)


aid=intent.getStringExtra("aid")
        dimensions=findViewById<EditText>(R.id.editTextDimension)
        color=findViewById<EditText>(R.id.editTextColor)
        price=findViewById<EditText>(R.id.editTextPrice)
        description=findViewById<EditText>(R.id.editTextDescription)
        loadingBar = ProgressDialog(this)

        //numberOrderTxt=findViewById<TextView>(R.id.numberOrderTxt)
        //val plusBtn=findViewById<ImageView>(R.id.plusBtn)
        //val minusBtn=findViewById<ImageView>(R.id.minusBtn)
        val addtocart=findViewById<AppCompatButton>(R.id.addToCart)



        orderImagesRef = FirebaseStorage.getInstance().reference.child("Order Images")

        orderRef = FirebaseDatabase.getInstance().reference.child("Artisans").child(aid!!).child("Pattern")
        orderRefTwo=FirebaseDatabase.getInstance().reference.child("Pattern")

        selectImage=findViewById<ImageView>(R.id.select_order_image)

//        plusBtn.setOnClickListener {
//            numberOrder = numberOrder!! + 1
//            numberOrderTxt!!.text = numberOrder.toString()
//        }
//
//        minusBtn.setOnClickListener {
//            if (numberOrder!! > 1) {
//                numberOrder = numberOrder!! - 1
//            }
//            numberOrderTxt!!.text = numberOrder.toString()
//        }

        selectImage!!.setOnClickListener(){
            OpenGallery()

        }
        addtocart.setOnClickListener(View.OnClickListener {
            //ValidateOrderData(numberOrder.toString())
            ValidateOrderData()
        })

    }
    private fun OpenGallery() {
        val galleryIntent= Intent()
        galleryIntent.action = Intent.ACTION_GET_CONTENT
        galleryIntent.type = "image/*"

        startActivityForResult(galleryIntent, GalleryPick)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GalleryPick && resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.data
            selectImage?.setImageURI(imageUri)
        }
    }
    private fun ValidateOrderData() {


        dimensioninput = dimensions?.text.toString()
        colorinput = color?.text.toString()
        priceinput= price?.text.toString()
        descriptioninput=description?.text.toString()


        if (imageUri == null) {
            Toast.makeText(this, "Pattern image is mandatory...", Toast.LENGTH_SHORT).show()
        }else if (TextUtils.isEmpty(dimensioninput)) {
            Toast.makeText(this, "Please write dimensions...", Toast.LENGTH_SHORT).show()
        }
        else if (TextUtils.isEmpty(colorinput)) {
            Toast.makeText(this, "Please write the color...", Toast.LENGTH_SHORT).show()
        }
        else if (TextUtils.isEmpty(descriptioninput)) {
            Toast.makeText(this, "Please write the description...", Toast.LENGTH_SHORT).show()
        }
        else if (TextUtils.isEmpty(priceinput)) {
            Toast.makeText(this, "Please write the price...", Toast.LENGTH_SHORT).show()
        }

        else {
            loadingBar?.setTitle("Adding pattern")
            loadingBar?.setMessage("Please wait, while we are adding the pattern.")
            loadingBar?.setCanceledOnTouchOutside(false)
            loadingBar?.show()
            StoreOrderInformation()
        }
    }

    private fun StoreOrderInformation() {
        loadingBar?.setTitle("Add New pattern")
        loadingBar?.setMessage("Dear Admin, please wait while we are adding the new pattern.")
        loadingBar?.setCanceledOnTouchOutside(false)
        loadingBar?.show()
        val calendar = Calendar.getInstance()

        val currentDate = SimpleDateFormat("MMM dd, yyyy")
        saveCurrentDate = currentDate.format(calendar.time)

        val currentTime = SimpleDateFormat("HH:mm:ss a")
        saveCurrentTime = currentTime.format(calendar.time)

        orderRandomKey = saveCurrentDate + saveCurrentTime
        val filePath: StorageReference = orderImagesRef!!.child(imageUri!!.lastPathSegment + orderRandomKey + ".jpg")
        val uploadTask = filePath.putFile(imageUri!!)
        uploadTask.addOnFailureListener { e ->
            val message = e.toString()
            Toast.makeText(this, "Error: $message", Toast.LENGTH_SHORT).show()

        }.addOnSuccessListener {
          //  Toast.makeText(this, "Event Image uploaded Successfully...", Toast.LENGTH_SHORT).show()
            val urlTask = uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                downloadImageUrl = filePath.downloadUrl.toString()
                filePath.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    downloadImageUrl = task.result.toString()
//                    Toast.makeText(
//                        this,
//                        "got the event image Url Successfully...",
//                        Toast.LENGTH_SHORT
//                    ).show()
                    SaveOrderInfoToDatabase()
                }
            }
        }
    }

    private fun SaveOrderInfoToDatabase() {

        var orderRefNew: DatabaseReference?=FirebaseDatabase.getInstance().reference.child("Artisans").child(aid!!)

        orderRefNew!!.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
               if(snapshot.exists()){
                   var artisanModel: ArtisanModel?=snapshot.getValue(ArtisanModel::class.java)
                   var category: String?=artisanModel!!.category

                   val orderMap = HashMap<String, Any>()
                   orderMap["oid"]=orderRandomKey!!
                   orderMap["dimensions"]=dimensioninput!!
                   orderMap["image"]=downloadImageUrl!!
                   orderMap["color"]=colorinput!!
                   orderMap["description"]=descriptioninput!!
                   orderMap["price"]=priceinput!!
                   orderMap["category"]=category!!

                   orderRef?.child(orderRandomKey!!)?.updateChildren(orderMap)
                       ?.addOnCompleteListener(OnCompleteListener<Void?> { task ->
                           if (task.isSuccessful) {
                               loadingBar!!.dismiss()
                               val intent = Intent(this@AddPattern, AdminHome::class.java)
                               startActivity(intent)

                               Toast.makeText(
                                   this@AddPattern,
                                   "Pattern is added successfully..",
                                   Toast.LENGTH_SHORT
                               ).show()
                           } else {
                               loadingBar!!.dismiss()
                               val message = task.exception.toString()
                               Toast.makeText(this@AddPattern, "Error: $message", Toast.LENGTH_SHORT).show()
                           }
                       })



                   orderRefTwo?.child(orderRandomKey!!)?.updateChildren(orderMap)
                       ?.addOnCompleteListener(OnCompleteListener<Void?> {

                       })

               }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })


    }
}