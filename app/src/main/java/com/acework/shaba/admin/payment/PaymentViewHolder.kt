package com.acework.shaba.admin.payment

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.ui.home.ItemClickListener

class PaymentViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener {
    var txtMpesaCode: TextView
    var txtNumber: TextView
    var txtDate: TextView
    var txtAmount: TextView
    private var listener: ItemClickListener? = null
    override fun onClick(v: View?) {
        listener?.onClick(v, adapterPosition, false)
    }

    init{
        txtMpesaCode=itemView.findViewById(R.id.mpesacode)
        txtNumber=itemView.findViewById(R.id.phonenumbermpesa)
        txtDate=itemView.findViewById(R.id.datempesa)
        txtAmount=itemView.findViewById(R.id.amountmpesa)
    }
}