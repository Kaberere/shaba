package com.acework.shaba.admin.payment

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.cardview.widget.CardView
import com.acework.shaba.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.text.SimpleDateFormat
import java.util.*

class AddPayment : AppCompatActivity() {
    private var mpesacode :EditText?=null
    private var number :EditText?=null
    private var date :TextView?=null
    private var amount :EditText?=null
    private var mDateSetListener: DatePickerDialog.OnDateSetListener? = null
    private var loadingBar: ProgressDialog? = null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null
    private var paymentRandomKey: String? = null
    private var paymentRef: DatabaseReference? = null
    private var paymentRefWood: DatabaseReference? = null
    private var datess: String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_payment)

        mpesacode=findViewById(R.id.mpesacodeadd)
        number=findViewById(R.id.mpesanumberadd)
        date=findViewById(R.id.mpesadateadd)
        val datecard=findViewById<CardView>(R.id.dateofmpesa);
        amount=findViewById(R.id.mpesamountadd)
        loadingBar = ProgressDialog(this)

        paymentRef = FirebaseDatabase.getInstance().reference.child("Payments")
        paymentRefWood = FirebaseDatabase.getInstance().reference.child("PaymentsWood")
        var addPayment : AppCompatButton=findViewById(R.id.addpayment)
        var addPaymentWood: AppCompatButton=findViewById(R.id.addpaymentWood)

        datecard.setOnClickListener(){

            Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT)
            val cal=Calendar.getInstance();
            val year: Int=cal.get(Calendar.YEAR)
            val month: Int=cal.get(Calendar.MONTH)
            val day: Int=cal.get(Calendar.DAY_OF_MONTH)

            val dialog = this?.let { it1 ->
                DatePickerDialog(
                    it1,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mDateSetListener,
                    year, month, day
                )
            }
            dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }

        mDateSetListener = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            var month = month
            month += 1
            val dates = "$month/$day/$year"
            date?.text = dates

            datess = date?.text.toString()
        }

        var weaver: String?="Weaver"
        var woodwork: String?="WoodWork"

        addPayment.setOnClickListener(View.OnClickListener {
            Payment(weaver!!)
        })

        addPaymentWood.setOnClickListener(View.OnClickListener {
            Payment(woodwork!!)
        })


    }

    private fun Payment(category: String) {

        val mpesacode: String = mpesacode?.text.toString()
        val number: String=number?.text.toString()
        val amount: String=amount?.text.toString()

        if (TextUtils.isEmpty(mpesacode)) {
            Toast.makeText(this, "Please write the mpesa code...", Toast.LENGTH_SHORT).show()
        } else if(TextUtils.isEmpty(number)) {
            Toast.makeText(this, "Please write the phone number...", Toast.LENGTH_SHORT).show()
        }
       else if(datess==null){
            Toast.makeText(this, "Please input the date...", Toast.LENGTH_SHORT).show()
        }
        else if (TextUtils.isEmpty(amount)) {
            Toast.makeText(this, "Please write the amount...", Toast.LENGTH_SHORT).show()
        }

        else {
       val date: String = datess!!

            ValidatePayment(mpesacode,number,date,amount,category)
        }
    }

    private fun ValidatePayment(
        mpesacode: String,
        number: String,
        date: String,
        amount: String,
        category: String
    ) {
        loadingBar?.setTitle("Add Payment")
        loadingBar?.setMessage("Please wait, while we are adding payment.")
        loadingBar?.setCanceledOnTouchOutside(false)
         loadingBar?.show()

        val calendar = Calendar.getInstance()

        val currentDate = SimpleDateFormat("MMM dd, yyyy")
        saveCurrentDate = currentDate.format(calendar.time)

        val currentTime = SimpleDateFormat("HH:mm:ss a")
        saveCurrentTime = currentTime.format(calendar.time)

        paymentRandomKey = saveCurrentDate + saveCurrentTime

        val paymentMap=HashMap<String, Any>()
        paymentMap["pid"]=paymentRandomKey!!
        paymentMap["mpesacode"]=mpesacode
        paymentMap["number"]=number
        paymentMap["date"]=date
        paymentMap["amount"]=amount

        if(category.equals("Weaver")) {

            paymentRef?.child(paymentRandomKey!!)?.updateChildren(paymentMap)
                ?.addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, PaymentActivity::class.java)
                        startActivity(intent)
                        loadingBar!!.dismiss()
                        Toast.makeText(
                            this,
                            "Payment is added successfully..",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    } else {
                        loadingBar!!.dismiss()
                        val message = task.exception.toString()
                        Toast.makeText(this, "Error: $message", Toast.LENGTH_SHORT).show()
                    }
                })

        }
        else if(category.equals("WoodWork")){
            paymentRefWood?.child(paymentRandomKey!!)?.updateChildren(paymentMap)
                ?.addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, PaymentActivity::class.java)
                        startActivity(intent)
                        loadingBar!!.dismiss()
                        Toast.makeText(
                            this,
                            "Payment is added successfully..",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    } else {
                        loadingBar!!.dismiss()
                        val message = task.exception.toString()
                        Toast.makeText(this, "Error: $message", Toast.LENGTH_SHORT).show()
                    }
                })
        }


    }
}