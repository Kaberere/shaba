package com.acework.shaba.ui.home.organize

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.ui.home.ItemClickListener

class CategoriesViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener
{
    var txtDate: TextView
    var txtTotal: TextView

    private var listener: ItemClickListener? = null
    fun setItemClickListener(listener: ItemClickListener?) {
        this.listener = listener
    }
    override fun onClick(v: View?) {
        listener?.onClick(v, adapterPosition, false)
    }
    init {

        txtTotal = itemView.findViewById<TextView>(R.id.totalCheckout)
        txtDate=itemView.findViewById<TextView>(R.id.dateCheckout)
        itemView.setOnClickListener(this)
    }
}