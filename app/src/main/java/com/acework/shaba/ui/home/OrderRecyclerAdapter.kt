package com.acework.shaba.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R


class OrderRecyclerAdapter
   // (private val images: IntArray,
    (private val orderList: List<OrderModel>,
     private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<OrderRecyclerAdapter.OrderViewHolder>() {

    //val images : Array<Int> = arrayOf()
    // constructor() : this(intArrayOf())


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.orders_layout, parent, false)

        return OrderViewHolder(view)

    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        //val image_id=images[position]
        val currentItem = orderList[position]
        // holder.imageView.setImageResource(image_id)
      //  holder.imageView.setImageResource(currentItem.image!!)
       // holder.txtOrderName.text = currentItem.orderno
        //  holder.txtOrderName.setText("Order :"+position)
//        holder.itemView.setOnClickListener(View.OnClickListener {
//
//            val intent= Intent(activity,Orders::class.java)
//
//        })
    }

    override fun getItemCount(): Int {
        // return images.size;
        return orderList.size
    }


    inner class OrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        var txtOrderName: TextView
        var imageView: ImageView

//    fun setItemClickListener(listener: ItemClickListener?) {
//        this.listener = listener
//    }
//

        init {
            imageView = itemView.findViewById<ImageView>(R.id.order_image)
            txtOrderName = itemView.findViewById<TextView>(R.id.order_name)
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }


    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}