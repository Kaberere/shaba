package com.acework.shaba.ui.home.organize

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.MainActivity
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CheckOutModel
import com.acework.shaba.home.HomeActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class CategoriesActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var auth: FirebaseAuth
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var checkoutRef: DatabaseReference? = null
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)


        // Initialize Firebase Auth
        auth = Firebase.auth

        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                println("User logged in")
            } else {
                println("User not logged in")
            }
        }

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar_homeC)
        setSupportActionBar(toolbar)

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layouttwo);

        val navigationView = findViewById<NavigationView>(R.id.nav_view)



        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)

        checkoutRef= FirebaseDatabase.getInstance().reference.child("CheckOut")

        recyclerView=findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager

        progressBar=findViewById(R.id.progress_bar_orders)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id: Int = item.getItemId()

        if (id == R.id.nav_home) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        else if(id == R.id.nav_cart){

            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
        else if(id==R.id.nav_logout){
            AuthUI.getInstance().signOut(this).addOnCompleteListener(OnCompleteListener {
                val intent = Intent(this, MainActivity::class.java)
                FirebaseAuth.getInstance().signOut();
                startActivity(intent)
            })

        }
        return true
    }
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.


        auth.addAuthStateListener(mAuthListener!!);

        var options = FirebaseRecyclerOptions.Builder<CheckOutModel>()
            .setQuery(checkoutRef!!, CheckOutModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CheckOutModel,CategoriesViewHolder> = object :
            FirebaseRecyclerAdapter<CheckOutModel,CategoriesViewHolder>(options){
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): CategoriesViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.categories_layout, parent, false)
                return CategoriesViewHolder(view)
            }

            override fun onBindViewHolder(categoriesViewHolder: CategoriesViewHolder, p1: Int, checkOutModel: CheckOutModel) {
                var cid=checkOutModel.cid
                var total=checkOutModel.total

                categoriesViewHolder.txtDate.text=checkOutModel.cid
                categoriesViewHolder.txtTotal.text=total

                progressBar!!.visibility = ProgressBar.INVISIBLE

                categoriesViewHolder.itemView.setOnClickListener(View.OnClickListener {
                    val intent=Intent(this@CategoriesActivity,HomeActivity::class.java)
                    intent.putExtra("cid",cid)
                    startActivity(intent)
                })
            }

        }
        recyclerView!!.adapter = adapter
        adapter.startListening()
    }
}