package com.acework.shaba.admin.orders.cart

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.`interface`.TextData
import com.acework.shaba.ui.home.ItemClickListener

class CartViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener{
    var image: ImageView
    var price: TextView
    var color: TextView
    var plusBtn: ImageView
    var minusBtn: ImageView
    var quantitynum: TextView
    var total: TextView
    private var listener: ItemClickListener? = null
    fun setItemClickListener(listener: ItemClickListener?) {
        this.listener = listener
    }
    override fun onClick(v: View?) {
        listener?.onClick(v, adapterPosition, false)
    }

    init{
        image=itemView.findViewById<ImageView>(R.id.cart_image)
        price=itemView.findViewById<TextView>(R.id.feeEachItem)
        color=itemView.findViewById<TextView>(R.id.cart_color)
        plusBtn=itemView.findViewById<ImageView>(R.id.plusCardBtn)
        minusBtn=itemView.findViewById<ImageView>(R.id.minusCardBtn)
        quantitynum=itemView.findViewById<TextView>(R.id.numberItemTxtCart)
        total=itemView.findViewById<TextView>(R.id.totalEachItem)


    }


    fun bindData(textData: TextData){
        quantitynum.text="${textData.itemCount}"
    }
}