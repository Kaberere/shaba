package com.acework.shaba.home

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.ui.home.ItemClickListener

class OrderArtisanViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener{
    var txtOrderName: TextView
    var txtPriority: TextView
    var imageView: ImageView

    private var listener: ItemClickListener? = null
    override fun onClick(v: View?) {
        listener?.onClick(v, adapterPosition, false)
    }

    init {
        imageView = itemView.findViewById<ImageView>(R.id.popular_image_artisan)
        txtOrderName = itemView.findViewById<TextView>(R.id.fee_artisan)
        txtPriority = itemView.findViewById<TextView>(R.id.priority)
    }
}