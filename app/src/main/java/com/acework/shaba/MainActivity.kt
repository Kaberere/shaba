package com.acework.shaba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var tabLayout : TabLayout? = findViewById<TabLayout>(R.id.tab_layout)
        val viewPager: ViewPager = findViewById<ViewPager>(R.id.view_pager)


        tabLayout!!.addTab(tabLayout.newTab().setText("Login"))

        tabLayout.addTab(tabLayout.newTab().setText("Admin"))

        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL


        val pagerAdapterHome = LoginAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter=pagerAdapterHome
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })


        tabLayout.translationY =300.toFloat()

        tabLayout.alpha=0.toFloat()

        tabLayout.animate().translationY(0.toFloat()).alpha(1.toFloat()).setDuration(1000).setStartDelay(100).start()



    }
}