package com.acework.shaba.ui.home.organize.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.admin.orders.OrderViewHolderTwo
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.home.OrderArtisanViewHolder
import com.acework.shaba.model.Users
import com.acework.shaba.ui.home.OrderRecyclerAdapter
import com.acework.shaba.ui.home.orders.Orders
import com.acework.shaba.ui.home.orders.OrdersTwo
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase

class OrderFragmentTwo: Fragment() {
    private lateinit var auth: FirebaseAuth
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var checkoutRef: DatabaseReference? = null
    private var checkoutRefWood: DatabaseReference? = null
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar?=null
    private var userRef: DatabaseReference? = null
    private var gridLayoutManager: GridLayoutManager?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_home, container, false)
        gridLayoutManager = GridLayoutManager(activity, 2)

        recyclerView=view.findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = gridLayoutManager

        val textgone: TextView =view.findViewById(R.id.textgone)

        progressBar=view.findViewById(R.id.progress_bar_orders)
        auth = Firebase.auth
        val user = auth.currentUser
        checkoutRef= FirebaseDatabase.getInstance().reference.child("CheckOutWeaver")
        checkoutRefWood= FirebaseDatabase.getInstance().reference.child("CheckOutWoodWork")
        if (user != null) {
            val phone = user.phoneNumber
            userRef = FirebaseDatabase.getInstance().reference.child("Users").child(phone!!)
            println("User logged in")
            userRef!!.addListenerForSingleValueEvent (object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    var users: Users? = snapshot.getValue(Users::class.java)
                    var category: String? = users!!.category

                    if (category!!.equals("Weaver")) {

                        checkoutRef!!.addValueEventListener(object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                if(snapshot.exists()){
                                    bindView(checkoutRef!!,category!!)
                                    textgone.visibility=View.INVISIBLE
                                    progressBar!!.visibility = ProgressBar.INVISIBLE
                                }
                                else{
                                    //Toast.makeText(activity,"Currently no orders available",Toast.LENGTH_LONG).show()
                                    progressBar!!.visibility = ProgressBar.INVISIBLE
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {
                                Toast.makeText(activity,"${error.message}",Toast.LENGTH_SHORT).show()
                            }

                        })

                    }
                    else if (category!!.equals("Woodwork")) {
                        checkoutRefWood!!.addValueEventListener(object :ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                if(snapshot.exists()){
                                    bindView(checkoutRefWood!!,category!!)
                                    textgone.visibility=View.INVISIBLE
                                    progressBar!!.visibility = ProgressBar.INVISIBLE
                                }
                                else{
                                    Toast.makeText(activity,"Currently no orders available",Toast.LENGTH_LONG).show()
                                    progressBar!!.visibility = ProgressBar.INVISIBLE
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {
                                Toast.makeText(activity,"${error.message}",Toast.LENGTH_SHORT).show()
                            }

                        })

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Toast.makeText(activity,"${error.message}", Toast.LENGTH_SHORT).show()
                }

            })

        }else {
            println("User not logged in")
        }

        return view

    }

    private fun bindView(checkoutRef: DatabaseReference, category: String) {
        var options= FirebaseRecyclerOptions.Builder<CartModel>()
            .setQuery(checkoutRef, CartModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CartModel, OrderArtisanViewHolder> = object:
        FirebaseRecyclerAdapter<CartModel,OrderArtisanViewHolder>(options){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderArtisanViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.content_artisan_home, parent, false)
                return OrderArtisanViewHolder(view)
            }

            override fun onBindViewHolder(orderViewHolderTwo: OrderArtisanViewHolder, i: Int, cartModel: CartModel) {
          val oid= cartModel.oid
                val category=cartModel.category
                val priority=cartModel.priority

                orderViewHolderTwo.txtOrderName.text=cartModel.price

                if(priority.equals("High")){
                    orderViewHolderTwo.txtPriority.isVisible= true
                }


                Glide.with(activity!!).load(cartModel.image).into(orderViewHolderTwo.imageView)

                orderViewHolderTwo.itemView.setOnClickListener(View.OnClickListener {
                    val intent= Intent(activity, OrdersTwo::class.java)
                    intent.putExtra("oid", oid)
                    intent.putExtra("category",category)
                    startActivity(intent)

                })

            }

        }

        recyclerView!!.adapter=adapter
        adapter.startListening()
    }
}