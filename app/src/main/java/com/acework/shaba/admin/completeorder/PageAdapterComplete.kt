package com.acework.shaba.admin.completeorder

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.acework.shaba.admin.completeorder.fragment.CompleteWeaverFragment
import com.acework.shaba.admin.completeorder.fragment.CompleteWoodFragment

class PageAdapterComplete internal constructor(myContext: Context, fm: FragmentManager, var totalTabs: Int) : FragmentStatePagerAdapter(fm) {
    override fun getCount(): Int {
        return totalTabs
    }

    override fun getItem(i: Int): Fragment {
        when (i) {
            0 -> {
                return CompleteWeaverFragment()
            }
            1 -> {
                return CompleteWoodFragment()
            }


        }
        throw IllegalStateException("position $i is invalid for this viewpager")

    }
}