package com.acework.shaba.home

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.acework.shaba.home.ui.gallery.Payments
import com.acework.shaba.ui.home.organize.fragments.OrderFragment
import com.acework.shaba.ui.home.organize.fragments.OrderFragmentTwo

class PageAdapterHome internal constructor(myContext: Context, fm: FragmentManager, var totalTabs: Int) : FragmentStatePagerAdapter(fm){
    override  fun getItem(i: Int): Fragment {
        when (i) {
            0 -> {
              //  return OrderFragment()
                return OrderFragmentTwo()
            }
            1 -> {
                return Payments()
            }


        }
        throw IllegalStateException("position $i is invalid for this viewpager")
    }

    override fun getCount(): Int {
        return totalTabs
    }
}