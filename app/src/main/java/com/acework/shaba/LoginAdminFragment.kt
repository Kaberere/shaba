package com.acework.shaba

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.acework.shaba.admin.AdminHome
import com.acework.shaba.admin.model.AdminModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class LoginAdminFragment: Fragment() {
    private var email: EditText?=null
    private var password: EditText?=null
    private var forgot: TextView?=null
    private val parentDbName = "Admins"
    private var loadingBar: ProgressDialog? = null
    private var adminRef: DatabaseReference? = null
    private lateinit var auth: FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.login_admin_fragment, container, false)
        email=view.findViewById<EditText>(R.id.emailAdmin)
        password=view.findViewById<EditText>(R.id.passAdmin)
        forgot=view.findViewById<TextView>(R.id.forgotPassAdmin)
        loadingBar = ProgressDialog(activity)

        val login=view.findViewById<AppCompatButton>(R.id.loginAdmin)

        email!!.translationX=800.toFloat()
        password!!.translationX=800.toFloat()
        forgot!!.translationX=800.toFloat()
        login.translationX=800.toFloat()

        email!!.alpha=0.toFloat()
        password!!.alpha=0.toFloat()
        forgot!!.alpha=0.toFloat()
        login.alpha=0.toFloat()


        email!!.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(300).start()
        password!!.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(500).start()
        forgot!!.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(500).start()
        login.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(700).start()
        auth = FirebaseAuth.getInstance()
        login.setOnClickListener {


            LoginAdmin()
        }

        return view
    }

//    public override fun onStart() {
//        super.onStart()
//        // Check if user is signed in (non-null) and update UI accordingly.
//        val currentUser = auth.currentUser
//        if(currentUser != null){
//            val intent= Intent(activity, AdminHome::class.java )
//            intent.putExtra("user",currentUser)
//            startActivity(intent)
//        }
//        else{
//            val email: String=email!!.text.toString()
//            val password: String=password!!.text.toString()
//            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener( OnCompleteListener{ task ->
//                if(task.isSuccessful){
//                    Toast.makeText(activity, "Successfully Registered", Toast.LENGTH_LONG).show()
//                    val intent = Intent(activity, AdminHome::class.java)
//                    startActivity(intent)
//
//                }else {
//                    Toast.makeText(activity, "Registration Failed", Toast.LENGTH_LONG).show()
//                }
//            })
//        }
//    }

    private fun LoginAdmin() {
        val email: String=email!!.text.toString()
        val password: String=password!!.text.toString()
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(activity, "Please write your email...", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(activity, "Please write your password...", Toast.LENGTH_SHORT).show()
        } else {
            loadingBar!!.setTitle("Login Account")
            loadingBar!!.setMessage("Please wait, while we are checking the credentials.")
            loadingBar!!.setCanceledOnTouchOutside(false)
            loadingBar!!.show()
            AllowAccessToAccount(email, password)
        }
    }

    private fun AllowAccessToAccount(email: String, password: String) {

        adminRef= FirebaseDatabase.getInstance().reference.child(parentDbName);

        adminRef!!.child("1").addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
if(snapshot.exists()){
    var adminModel: AdminModel? = snapshot.getValue(AdminModel::class.java)
    if(adminModel!!.email.equals(email))
    {
        if((adminModel!!.password).equals(password)){
            val intent= Intent(activity, AdminHome::class.java )
            startActivity(intent)
            activity!!.finish()
        }
        else{
            Toast.makeText(activity, "Wrong password...", Toast.LENGTH_SHORT).show()
        }
    }
    else{
        Toast.makeText(activity, "Wrong email address...", Toast.LENGTH_SHORT).show()
    }
}
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

    }
}