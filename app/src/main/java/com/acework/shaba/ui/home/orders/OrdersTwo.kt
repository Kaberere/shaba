package com.acework.shaba.ui.home.orders

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.home.HomeActivity
import com.acework.shaba.ui.home.HomeActivityTwo
import com.bumptech.glide.Glide
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import java.util.ArrayList

class OrdersTwo : AppCompatActivity() {
    private var image: ImageView? = null
    private var dimensions: TextView?= null
    private var color: TextView?= null
    private var price: TextView? =null
    private var quantity: TextView? =null
    private var cartRef: DatabaseReference? = null
    private var orderno: String? =null
    private var oid: String?=null
    private var key: String?=null
    private var category: String?=null
    private var description: TextView?=null
    private var completeorderRef: DatabaseReference?=null
    private var imageSlider: ImageSlider?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders)

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar_orderss)
        setSupportActionBar(toolbar)

        supportActionBar?.title = "Pattern"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        oid = intent.getStringExtra("oid")
        category=intent.getStringExtra("category")

       // image = findViewById<ImageView>(R.id.image_order_single)
        dimensions=findViewById<TextView>(R.id.single_dimension)
        color = findViewById<TextView>(R.id.single_color)
        description=findViewById<TextView>(R.id.single_description)
        price= findViewById<TextView>(R.id.single_price)
        quantity=findViewById<TextView>(R.id.single_quantity)
        imageSlider=findViewById(R.id.image_slider_artisan)

        //val goback: AppCompatButton =findViewById(R.id.goback)

        val floatingbar=findViewById<FloatingActionButton>(R.id.completeorderfabOrders)

        if(category.equals("Weaver")) {
            cartRef = FirebaseDatabase.getInstance().reference.child("CheckOutWeaver")
        }
        else if(category.equals("Woodwork")){
            cartRef = FirebaseDatabase.getInstance().reference.child("CheckOutWoodWork")
        }

        getDetails(oid)

//        goback.setOnClickListener(View.OnClickListener {
//            val intent = Intent(this, HomeActivity::class.java)
//            intent.putExtra("cid",key)
//            startActivity(intent)
//            finish()
//        })

        floatingbar.setOnClickListener(View.OnClickListener {
            if(category.equals("Weaver")) {
                completeorderRef=FirebaseDatabase.getInstance().reference.child("CompleteOrder")
                moveRecord(cartRef!!.child(oid!!),completeorderRef!!.child(oid!!))
            }
            else if(category.equals("Woodwork")){
                completeorderRef=FirebaseDatabase.getInstance().reference.child("CompleteOrderWood")
                moveRecord(cartRef!!.child(oid!!),completeorderRef!!.child(oid!!))
            }


            val intent=Intent(this,HomeActivity::class.java)
            startActivity(intent)
            finish()

            cartRef!!.child(oid!!).removeValue()
        })
    }

    private fun getDetails(oid: String?) {
        val imageList = ArrayList<SlideModel>()
        if(oid!=null) {
            cartRef!!.child(oid).addValueEventListener(object :
                ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        var cartModel: CartModel?=snapshot.getValue(CartModel::class.java)
                        dimensions!!.text=cartModel!!.dimensions
                        price!!.text=cartModel.price
                        quantity!!.text=cartModel.quantity
                        color!!.text=cartModel.color
                        description!!.text=cartModel.description
                     //   Glide.with(this@OrdersTwo).load(cartModel.image).into(image!!)
                        imageList.add(SlideModel(cartModel.image))
                        cartRef!!.child(oid).child("images").addListenerForSingleValueEvent(object:ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {

                                for(datasnashot in snapshot.children){

                                    imageList.add(SlideModel(datasnashot.child("image").value.toString()))

                                }
                                imageSlider!!.setImageList(imageList, ScaleTypes.CENTER_CROP)
                                imageSlider!!.stopSliding()
                            }

                            override fun onCancelled(error: DatabaseError) {
                                TODO("Not yet implemented")
                            }

                        })

                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })
        }
        }

    private fun moveRecord(fromPath: DatabaseReference, toPath: DatabaseReference)
    {
        val valueEventListener: ValueEventListener = object : ValueEventListener
        {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                toPath.setValue(dataSnapshot.value).addOnCompleteListener { task ->
                    if (task.isComplete) {
                        Log.d("TAG", "Success!")
                        Toast.makeText(
                            this@OrdersTwo,
                            "Completed task successfully..",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Log.d("TAG", "Copy failed!")
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        }
        fromPath.addListenerForSingleValueEvent(valueEventListener)
    }
}