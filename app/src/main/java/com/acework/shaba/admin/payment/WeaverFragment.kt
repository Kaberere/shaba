package com.acework.shaba.admin.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.ui.payments.PaymentModel
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class WeaverFragment : Fragment() {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar?=null
    private var paymentRef: DatabaseReference? = null
    private var textgone: TextView?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?{
        val view=inflater.inflate(R.layout.fragment_home, container, false)

        paymentRef = FirebaseDatabase.getInstance().reference.child("Payments")
        recyclerView=view.findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        recyclerView!!.layoutManager = layoutManager

        progressBar=view.findViewById(R.id.progress_bar_orders)
         textgone =view.findViewById(R.id.textgone)

        return view
    }

    override fun onStart() {
        super.onStart()

        var options= FirebaseRecyclerOptions.Builder<PaymentModel>()
            .setQuery(paymentRef!!, PaymentModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<PaymentModel, PaymentViewHolder> = object :
            FirebaseRecyclerAdapter<PaymentModel, PaymentViewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.payment_layout, parent, false)
                return PaymentViewHolder(view)
            }

            override fun onBindViewHolder(paymentViewHolder: PaymentViewHolder, p1: Int, paymentModel: PaymentModel) {
                val pid = paymentModel.pid

                paymentViewHolder.txtMpesaCode.text=paymentModel.mpesacode
                paymentViewHolder.txtNumber.text=paymentModel.number
                paymentViewHolder.txtDate.text=paymentModel.date
                paymentViewHolder.txtAmount.text=paymentModel.amount
                progressBar!!.visibility = ProgressBar.INVISIBLE
            }

        }

        recyclerView!!.adapter=adapter
        adapter.startListening()

        textgone!!.visibility=View.INVISIBLE

    }
}