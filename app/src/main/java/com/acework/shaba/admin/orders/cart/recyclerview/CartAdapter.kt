package com.acework.shaba.admin.orders.cart.recyclerview

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.admin.orders.cart.CartViewHolder
import com.acework.shaba.admin.orders.cart.`interface`.ChangeNumberItemsListener
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class CartAdapter (
    private val mCart: List<CartModel>,
    private val context: Context,
    private val changeNumberItemsListener: ChangeNumberItemsListener
    )
    : RecyclerView.Adapter<CartViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_card, parent, false)

        return CartViewHolder(view)
    }

    override fun onBindViewHolder(cartViewHolder: CartViewHolder, position: Int) {
        val cartModel=mCart[position]
        cartViewHolder.color.text=cartModel.color
        cartViewHolder.price.text=cartModel.price
        cartViewHolder.quantitynum.text=cartModel.quantity
        Glide.with(context).load(cartModel.image).into(cartViewHolder.image)

        var feeOne: Int=Integer.parseInt(cartModel.price)
        var quantityone: Int=Integer.parseInt(cartViewHolder.quantitynum.text .toString())

       var totals: Int=quantityone * feeOne
        cartViewHolder.total.text=totals.toString()

        //var numberOrder: Int?=0
        var totalprice: Int=0;
        val oid = cartModel.oid
        var cartRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("CartOrder")
        var numberOrder: Int=Integer.parseInt(cartViewHolder.quantitynum.text .toString())
        var totalsEach: Int=Integer.parseInt(cartViewHolder.total.text.toString())
        cartViewHolder.plusBtn.setOnClickListener(View.OnClickListener {

            numberOrder = numberOrder!! + 1
            cartViewHolder.quantitynum.text = numberOrder.toString()


            NewPost(cartViewHolder.quantitynum.text .toString(),oid)

//            var feeOne: Int=Integer.parseInt(cartModel.price)
//            var quantityone: Int=Integer.parseInt(cartModel.quantity)
//
//            totals=(quantityone * feeOne)
            //totalprice=totalprice-totalsEach
            notifyDataSetChanged()
            changeNumberItemsListener.changed()

        })

        cartViewHolder.minusBtn.setOnClickListener (View.OnClickListener {
            //var numberOrder: Int?=1
        //  var  numberOrder:Int =Integer.parseInt(cartViewHolder.quantitynum.text .toString())
            if(numberOrder ==1) {

            cartRef!!.child(oid!!).removeValue().addOnCompleteListener(
                OnCompleteListener { task ->
                    if(task.isSuccessful)
                    {
                        Toast.makeText(
                            context,
                            "Item removed successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })

        }
            else{
               // var totalsEach: Int=Integer.parseInt(cartViewHolder.total.text.toString())

                numberOrder = numberOrder!! - 1
                cartViewHolder.quantitynum.text = numberOrder.toString()

                NewPost(cartViewHolder.quantitynum.text .toString(),oid)

//                var feeOne: Int=Integer.parseInt(cartModel.price)
//                var quantityone: Int=Integer.parseInt(cartModel.quantity)
//
//                totals=(quantityone * feeOne)
               // totalprice=totalprice-totalsEach
                notifyDataSetChanged()
                changeNumberItemsListener.changed()

            }

            notifyItemRemoved(position)
            changeNumberItemsListener.changed()
        })


    }

    override fun getItemCount(): Int {
return mCart.size
    }



    private fun NewPost(quantity: String, oid: String?) {
       var cartRef: DatabaseReference= FirebaseDatabase.getInstance().reference.child("CartOrder")

        cartRef!!.child(oid!!).child("quantity").setValue(quantity).addOnSuccessListener {
            Log.d("CartListActivity","Successful quantitiy update")
        }
            .addOnFailureListener{
                Log.d("CartListActivity","Failure quantitiy update")
            }

    }
}