package com.acework.shaba.admin.payment

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.acework.shaba.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.DatabaseReference

class PaymentActivity : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var paymentRef: DatabaseReference? = null
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        val floatingbar= findViewById<FloatingActionButton>(R.id.fabpayment)

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbartPayment)
        setSupportActionBar(toolbar)

        supportActionBar?.title = "Payment"

        var tabLayout: TabLayout? = findViewById<TabLayout>(R.id.tabLayoutPayment)
        tabLayout!!.addTab(tabLayout.newTab().setText("Weaver"))

        tabLayout.addTab(tabLayout.newTab().setText("Woodwork"))

        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.tabTextColors = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"))

        val viewPager: ViewPager = findViewById<ViewPager>(R.id.pagerPayment)
        val pagerAdapterPayment = PageAdapterPayment(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = pagerAdapterPayment
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
//        paymentRef = FirebaseDatabase.getInstance().reference.child("Payments")
//
//        recyclerView=findViewById(R.id.paymentRecylerview)
//        recyclerView!!.setHasFixedSize(true)
//        layoutManager = LinearLayoutManager(this)
//        recyclerView!!.layoutManager = layoutManager


        floatingbar.setOnClickListener(View.OnClickListener {
            val intent= Intent(this, AddPayment::class.java)
            startActivity(intent)
        })

    }

//    override fun onStart() {
//        super.onStart()
//
//        var options= FirebaseRecyclerOptions.Builder<PaymentModel>()
//            .setQuery(paymentRef!!, PaymentModel::class.java)
//            .build()
//
//        var adapter: FirebaseRecyclerAdapter<PaymentModel,PaymentViewHolder> = object :
//        FirebaseRecyclerAdapter<PaymentModel,PaymentViewHolder>(options) {
//            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
//                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.payment_layout, parent, false)
//                return PaymentViewHolder(view)
//            }
//
//            override fun onBindViewHolder(paymentViewHolder: PaymentViewHolder, p1: Int, paymentModel: PaymentModel) {
//                val pid = paymentModel.pid
//
//                paymentViewHolder.txtMpesaCode.text=paymentModel.mpesacode
//                paymentViewHolder.txtNumber.text=paymentModel.number
//                paymentViewHolder.txtDate.text=paymentModel.date
//                paymentViewHolder.txtAmount.text=paymentModel.amount
//            }
//
//        }
//
//        recyclerView!!.adapter=adapter
//        adapter.startListening()
//
//    }
}