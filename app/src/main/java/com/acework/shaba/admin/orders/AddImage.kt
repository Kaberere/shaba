package com.acework.shaba.admin.orders

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.acework.shaba.R
import com.acework.shaba.admin.AdminHome
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AddImage : AppCompatActivity() {
    private var selectImage: ImageView?=null
    private var GalleryPick: Int=1
    private var imageUri: Uri?=null
    private var orderImagesRef: StorageReference? = null
    private  var downloadImageUrl:kotlin.String? = null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null
    private var orderRandomKey: String? = null
    private var oid: String?=null
    private var orderRef: DatabaseReference? = null
    private var addImage: AppCompatButton?=null
    private var loadingBar: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_image)

        selectImage=findViewById<ImageView>(R.id.select_additional_image)
        oid = intent.getStringExtra("oid")
        addImage=findViewById<AppCompatButton>(R.id.addImageAgain)
        loadingBar = ProgressDialog(this)

        orderImagesRef = FirebaseStorage.getInstance().reference.child("Order Images")
        orderRef= FirebaseDatabase.getInstance().reference.child("Pattern")

        selectImage!!.setOnClickListener(View.OnClickListener{
            OpenGallery()
        })
        addImage?.setOnClickListener(View.OnClickListener {
ValidateOrderData()
        })

    }
    private fun OpenGallery() {
        val galleryIntent= Intent()
        galleryIntent.action = Intent.ACTION_GET_CONTENT
        galleryIntent.type = "image/*"

        startActivityForResult(galleryIntent, GalleryPick)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GalleryPick && resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.data
            selectImage?.setImageURI(imageUri)
        }
    }

    private fun ValidateOrderData(){
        if(imageUri == null){
            Toast.makeText(this, "Pattern image is mandatory...", Toast.LENGTH_SHORT).show()
        }
        else{
            loadingBar?.setTitle("Adding Image")
            loadingBar?.setMessage("Please wait, while we are adding the image.")
            loadingBar?.setCanceledOnTouchOutside(false)
            loadingBar?.show()
            StoreImageInformation()
        }

    }

    private fun StoreImageInformation(){
        val calendar = Calendar.getInstance()

        val currentDate = SimpleDateFormat("MMM dd, yyyy")
        saveCurrentDate = currentDate.format(calendar.time)

        val currentTime = SimpleDateFormat("HH:mm:ss a")
        saveCurrentTime = currentTime.format(calendar.time)

        orderRandomKey = saveCurrentDate + saveCurrentTime

        val filePath: StorageReference = orderImagesRef!!.child(imageUri!!.lastPathSegment + orderRandomKey + ".jpg")
        val uploadTask = filePath.putFile(imageUri!!)

        uploadTask.addOnFailureListener { e ->
            val message = e.toString()
            Toast.makeText(this, "Error: $message", Toast.LENGTH_SHORT).show()

        }.addOnSuccessListener {
            //  Toast.makeText(this, "Event Image uploaded Successfully...", Toast.LENGTH_SHORT).show()
            val urlTask = uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                downloadImageUrl = filePath.downloadUrl.toString()
                filePath.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    downloadImageUrl = task.result.toString()
//                    Toast.makeText(
//                        this,
//                        "got the event image Url Successfully...",
//                        Toast.LENGTH_SHORT
//                    ).show()
                    SaveOrderInfoToDatabase()
                }
            }
        }
    }

    private fun SaveOrderInfoToDatabase(){

        val imageMap=HashMap<String, Any>()
        imageMap["image"]=downloadImageUrl!!
        imageMap["iid"]=orderRandomKey!!

        orderRef?.child(oid!!)?.child("images")?.child(orderRandomKey!!)?.updateChildren(imageMap)?.addOnCompleteListener(OnCompleteListener {task->
            if (task.isSuccessful) {
                loadingBar!!.dismiss()
                val intent = Intent(this@AddImage, AdminHome::class.java)
                startActivity(intent)

                Toast.makeText(
                    this@AddImage,
                    "Image is added successfully..",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                loadingBar!!.dismiss()
                val message = task.exception.toString()
                Toast.makeText(this@AddImage, "Error: $message", Toast.LENGTH_SHORT).show()
            }
        })

    }
}