package com.acework.shaba.admin.orders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.ui.home.ItemClickListener

class OrderViewHolderTwo (itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener{
    var txtOrderName: TextView
    var imageView: ImageView

    private var listener: ItemClickListener? = null
    fun setItemClickListener(listener: ItemClickListener?) {
        this.listener = listener
    }
    override fun onClick(v: View?) {
        listener?.onClick(v, adapterPosition, false)
    }
    init {
        imageView = itemView.findViewById<ImageView>(R.id.popular_image)
        txtOrderName = itemView.findViewById<TextView>(R.id.fee)
    }
}