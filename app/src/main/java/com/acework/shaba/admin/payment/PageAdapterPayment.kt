package com.acework.shaba.admin.payment

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class PageAdapterPayment internal constructor(myContext: Context, fm: FragmentManager, var totalTabs: Int) : FragmentStatePagerAdapter(fm){
    override fun getCount(): Int {
        return totalTabs
    }

    override fun getItem(i: Int): Fragment {
        when (i) {
            0 -> {
                return WeaverFragment()
            }
            1 -> {
                return WoodFragment()
            }


        }
        throw IllegalStateException("position $i is invalid for this viewpager")
    }
}