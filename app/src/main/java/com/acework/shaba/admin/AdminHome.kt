package com.acework.shaba.admin

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.ui.AppBarConfiguration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.acework.shaba.MainActivity
import com.acework.shaba.R
import com.acework.shaba.admin.completeorder.CompleteOrders
import com.acework.shaba.admin.model.ArtisanModel
import com.acework.shaba.admin.orders.AddPattern
import com.acework.shaba.admin.orders.OrderViewHolderTwo
import com.acework.shaba.admin.orders.ShowDetailsActivity
import com.acework.shaba.admin.orders.cart.CartListActivity
import com.acework.shaba.admin.payment.PaymentActivity
import com.acework.shaba.databinding.ActivityHomeBinding
import com.acework.shaba.ui.home.OrderModel
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class AdminHome : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityHomeBinding
    private var recyclerView: RecyclerView? = null
    private var recyclerViewTwo: RecyclerView? = null
    //private var aid: String? = null
    //private var oid: String? = null
    private var name: String?=null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var gridLayoutManager: GridLayoutManager?=null

    //private var artisanRecyclerAdapter: ArtisanRecyclerAdapter?=null
    private var artisanRef: DatabaseReference? = null
    private var orderRef: DatabaseReference? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_home)


        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbart)
        setSupportActionBar(toolbar)

        supportActionBar?.title = "Admin Panel"


        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout);

        val navigationView = findViewById<NavigationView>(R.id.nav_view)

        val floatingbar= findViewById<FloatingActionButton>(R.id.fab)

        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)

        artisanRef =FirebaseDatabase.getInstance().reference.child("Artisans")
       // orderRef=FirebaseDatabase.getInstance().reference.child("Pattern")

        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView=findViewById(R.id.category_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = linearLayoutManager

        //val linearLayoutManagerT = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        gridLayoutManager = GridLayoutManager(this, 2)

        recyclerViewTwo=findViewById(R.id.orders_list_two)
        recyclerViewTwo!!.setHasFixedSize(true)
        recyclerViewTwo!!.layoutManager = gridLayoutManager

        floatingbar.setOnClickListener(View.OnClickListener {
            val intent=Intent(this@AdminHome,AddArtisan::class.java)
            startActivity(intent)
        })


    }
    override fun onStart() {
        super.onStart()

        // Category Recyclerview
        var options= FirebaseRecyclerOptions.Builder<ArtisanModel>()
            .setQuery(artisanRef!!, ArtisanModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<ArtisanModel, ArtisanViewHolderTwo> = object :
            FirebaseRecyclerAdapter<ArtisanModel, ArtisanViewHolderTwo>(options) {
            protected override fun onBindViewHolder(artisanViewHolder: ArtisanViewHolderTwo, i: Int, artisanModel: ArtisanModel) {
               val aid = artisanModel.aid
                name=artisanModel.category


                artisanViewHolder.txtArtisanName.text=artisanModel.category

              //  Picasso.get().load(artisanModel.image).into(artisanViewHolder.imageView)
Glide.with(this@AdminHome).load(artisanModel.image).into(artisanViewHolder.imageView)
                artisanViewHolder.itemView.setOnClickListener(View.OnClickListener {
                    val intent = Intent(this@AdminHome, AddPattern::class.java)
                    intent.putExtra("aid", aid)
                    startActivity(intent)
                   val RootRef: DatabaseReference = FirebaseDatabase.getInstance().reference
                })

                orderRef=FirebaseDatabase.getInstance().reference.child("Pattern")

                //Order cart_list
                var optionsTwo= FirebaseRecyclerOptions.Builder<OrderModel>()
                    .setQuery(orderRef!!, OrderModel::class.java)
                    .build()

                var adapterTwo: FirebaseRecyclerAdapter<OrderModel, OrderViewHolderTwo> = object :
                    FirebaseRecyclerAdapter<OrderModel, OrderViewHolderTwo>(optionsTwo) {
                    protected override fun onBindViewHolder(orderViewHolderTwo: OrderViewHolderTwo, i: Int, orderModel: OrderModel) {
                        val oid = orderModel.oid

                        orderViewHolderTwo.txtOrderName.text=orderModel.price

                        //  Picasso.get().load(artisanModel.image).into(artisanViewHolder.imageView)
                        Glide.with(this@AdminHome).load(orderModel.image).into(orderViewHolderTwo.imageView)
                        orderViewHolderTwo.itemView.setOnClickListener(View.OnClickListener {
                            val intent = Intent(this@AdminHome, ShowDetailsActivity::class.java)
                            intent.putExtra("oid", oid)
                            intent.putExtra("aid",aid)
                            startActivity(intent)

                        })
                    }

                    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolderTwo {
                        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.content_admin_popular, parent, false)
                        return OrderViewHolderTwo(view)
                    }
                }

                recyclerViewTwo!!.adapter = adapterTwo
                adapterTwo.startListening()

            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtisanViewHolderTwo {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.content_admin_home, parent, false)
                return ArtisanViewHolderTwo(view)
            }
        }

        recyclerView!!.adapter = adapter
        adapter.startListening()


    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        // menuInflater.inflate(R.menu.search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        /*  if (id == R.id.action_settings) {
            return true;
        }
*/
//        if (id == R.id.action_search) {
//            /*val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:0722201368"))
//            startActivity(intent)*/
//        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {


        val id: Int = item.itemId

        if (id == R.id.nav_home) {
            val intent = Intent(this@AdminHome, AdminHome::class.java)
            startActivity(intent)
        }
       else if(id == R.id.nav_cart){
            val intent = Intent(this@AdminHome, CartListActivity::class.java)
            startActivity(intent)
        }
        else if(id == R.id.nav_payment){
            val intent = Intent(this@AdminHome, PaymentActivity::class.java)
            startActivity(intent)
        }
        else if(id == R.id.nav_complete){
            val intent = Intent(this@AdminHome, CompleteOrders::class.java)
            startActivity(intent)
        }
        else if(id==R.id.nav_logout){
            val intent = Intent(this@AdminHome, MainActivity::class.java)
            startActivity(intent)
        }

        return true
    }



}