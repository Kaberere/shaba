package com.acework.shaba.ui.home.organize.fragments
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CheckOutModel
import com.acework.shaba.model.Users
import com.acework.shaba.ui.home.HomeActivityTwo
import com.acework.shaba.ui.home.organize.CategoriesViewHolder
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase

class OrderFragment  : Fragment(){
    private lateinit var auth: FirebaseAuth
    private var mAuthListener: AuthStateListener? = null
    private var checkoutRef: DatabaseReference? = null
    private var checkoutRefWood: DatabaseReference? = null
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar?=null
    private var userRef: DatabaseReference? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_home, container, false)
        // Initialize Firebase Auth
        auth = Firebase.auth


        checkoutRef= FirebaseDatabase.getInstance().reference.child("CheckOut")

        checkoutRefWood= FirebaseDatabase.getInstance().reference.child("CheckOutWood")

        recyclerView=view.findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        recyclerView!!.layoutManager = layoutManager

        val textgone: TextView =view.findViewById(R.id.textgone)

        progressBar=view.findViewById(R.id.progress_bar_orders)
            val user = auth.currentUser
            if (user != null) {
                val phone=user.phoneNumber
                userRef= FirebaseDatabase.getInstance().reference.child("Users").child(phone!!)
                println("User logged in")

                userRef!!.addListenerForSingleValueEvent (object : ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {

                        var users: Users?=snapshot.getValue(Users::class.java)
                        var category: String?=users!!.category

                        if(category!!.equals("Weaver")){
                            checkoutRef!!.addValueEventListener(object :ValueEventListener{
                                override fun onDataChange(snapshot: DataSnapshot) {
                                    if(snapshot.exists()){
                                        bindView(checkoutRef!!,category!!)
                                        textgone.visibility=View.INVISIBLE
                                        progressBar!!.visibility = ProgressBar.INVISIBLE
                                    }
                                    else{
                                        //Toast.makeText(activity,"Currently no orders available",Toast.LENGTH_LONG).show()
                                        progressBar!!.visibility = ProgressBar.INVISIBLE
                                    }
                                }

                                override fun onCancelled(error: DatabaseError) {
                                    Toast.makeText(activity,"${error.message}",Toast.LENGTH_SHORT).show()
                                }

                            })

                        }
                        else if(category!!.equals("Wood work")){

                            checkoutRefWood!!.addValueEventListener(object :ValueEventListener{
                                override fun onDataChange(snapshot: DataSnapshot) {
                                    if(snapshot.exists()){
                                        bindView(checkoutRefWood!!,category!!)
                                        textgone.visibility=View.INVISIBLE
                                        progressBar!!.visibility = ProgressBar.INVISIBLE
                                    }
                                    else{
                                        Toast.makeText(activity,"Currently no orders available",Toast.LENGTH_LONG).show()
                                        progressBar!!.visibility = ProgressBar.INVISIBLE
                                    }
                                }

                                override fun onCancelled(error: DatabaseError) {
                                    Toast.makeText(activity,"${error.message}",Toast.LENGTH_SHORT).show()
                                }

                            })

                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Toast.makeText(activity,"${error.message}",Toast.LENGTH_SHORT).show()
                    }
                })
            } else {
                println("User not logged in")
            }



        return view


    }

    private fun bindView(checkoutRef: DatabaseReference, category: String) {

        var options = FirebaseRecyclerOptions.Builder<CheckOutModel>()
            .setQuery(checkoutRef, CheckOutModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CheckOutModel, CategoriesViewHolder> = object :
            FirebaseRecyclerAdapter<CheckOutModel, CategoriesViewHolder>(options){
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): CategoriesViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.categories_layout, parent, false)
                return CategoriesViewHolder(view)
            }

            override fun onBindViewHolder(categoriesViewHolder: CategoriesViewHolder, p1: Int, checkOutModel: CheckOutModel) {
                var cid=checkOutModel.cid
                var total=checkOutModel.total

                categoriesViewHolder.txtDate.text=checkOutModel.cid
                categoriesViewHolder.txtTotal.text=total

                progressBar!!.visibility = ProgressBar.INVISIBLE

                categoriesViewHolder.itemView.setOnClickListener(View.OnClickListener {
                    val intent= Intent(activity, HomeActivityTwo::class.java)
                    intent.putExtra("cid",cid)
                    intent.putExtra("category",category)
                    startActivity(intent)
                })
            }

        }
        recyclerView!!.adapter = adapter
        adapter.startListening()
    }
    }

