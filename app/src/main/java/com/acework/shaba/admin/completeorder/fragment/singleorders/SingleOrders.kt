package com.acework.shaba.admin.completeorder.fragment.singleorders

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.ui.home.OrderViewHolderTwoT
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class SingleOrders : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar? = null
    private var cid: String?=null
    private var category:String?=null
    private var cartRef: DatabaseReference? = null
    private var orderRef: DatabaseReference?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_orders)


        supportActionBar?.title = "Single Order"

        progressBar = findViewById<ProgressBar>(R.id.progress_bar_orders)
        recyclerView=findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager

        val floatingbar= findViewById<FloatingActionButton>(R.id.deleteorderfab)

        cid= intent!!.getStringExtra("cid")
        category=intent!!.getStringExtra("category")

        if(category.equals("Weaver")) {
            cartRef = FirebaseDatabase.getInstance().reference.child("CompleteOrder")
            
        }
        else if(category.equals("Wood work")){
            cartRef = FirebaseDatabase.getInstance().reference.child("CompleteOrderWood")

        }

        orderRef=cartRef!!.child(cid!!).child("Order")

        bindView(orderRef)

        floatingbar.setOnClickListener(View.OnClickListener {
            if(category.equals("Weaver")) {
                cartRef!!.child(cid!!).removeValue()
                Toast.makeText(this@SingleOrders,"Order deleted successfully",Toast.LENGTH_SHORT).show()
                finish()
            }
            else if(category.equals("Wood work")){
                cartRef!!.child(cid!!).removeValue()
                Toast.makeText(this@SingleOrders,"Order deleted successfully",Toast.LENGTH_SHORT).show()
                finish()
            }
        })
    }

    private fun bindView(orderRef: DatabaseReference?) {


        var options = FirebaseRecyclerOptions.Builder<CartModel>()
            .setQuery(orderRef!!, CartModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT> = object :
            FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT>(options) {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): OrderViewHolderTwoT {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.orders_layout, parent, false)
                return OrderViewHolderTwoT(view)
            }

            override fun onBindViewHolder(
                orderViewHolderTwoT: OrderViewHolderTwoT,
                p1: Int,
                cartModel: CartModel
            ) {
                val oid = cartModel.oid

                orderViewHolderTwoT.txtOrderName.text = cartModel.color
                orderViewHolderTwoT.txtDimensions.text = cartModel.dimensions
                orderViewHolderTwoT.txtDate.text = cartModel.oid

                Glide.with(this@SingleOrders).load(cartModel.image)
                    .into(orderViewHolderTwoT.imageView)

                orderViewHolderTwoT.itemView.setOnClickListener(View.OnClickListener {
                    val intent = Intent(this@SingleOrders, OrdersS::class.java)
                    intent.putExtra("oid", oid)
                    intent.putExtra("key",cid)
                    intent.putExtra("category",category)
                    startActivity(intent)
                })
            }

        }

        recyclerView!!.adapter = adapter
        adapter.startListening()
    }
}