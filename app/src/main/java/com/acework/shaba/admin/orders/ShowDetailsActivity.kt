package com.acework.shaba.admin.orders

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartListActivity
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.ui.home.OrderModel
import com.acework.shaba.ui.home.model.ImageModel
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ShowDetailsActivity : AppCompatActivity() {
    private var image: ImageView? = null
    private var dimensions: TextView?= null
    private var color: TextView?= null
    private var price: TextView? =null
    private var totalPrice: Int? =null
    private var description: TextView? =null
    private var numberOrderTxt: TextView?=null
    private var numberOrder: Int?=1
    private var oid: String?=null
    private var aid: String?=null
    private var orderRef: DatabaseReference?=null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null
    private var cartRandomKey: String? = null
    private var cartRef: DatabaseReference?=null
    private var artisanRef: DatabaseReference?=null
    private var ratingBar: RatingBar? =null
    private var imagestring: String?=null
    private var cartModel: CartModel?=null
    private var addImageButton: AppCompatButton?=null
    private var imageSlider: ImageSlider?=null
    private var priority: CheckBox?= null
    private var highPriority: String?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_details)

        //managementCart = ManagementCart(this)

        oid = intent.getStringExtra("oid")
        aid=intent.getStringExtra("aid")
       // image=findViewById(R.id.image_order_details)
        dimensions=findViewById(R.id.single_dimension_details)
        color=findViewById(R.id.single_color_details)
        description=findViewById(R.id.single_description_details)
        price=findViewById(R.id.single_price_details)
        ratingBar=findViewById(R.id.ratingdetails)
        numberOrderTxt=findViewById(R.id.numberOrderTxtDetails)
        addImageButton=findViewById(R.id.addAnotherImage)
        imageSlider=findViewById( R.id.image_slider)
        priority = findViewById(R.id.checkbox_priority)


        val plusBtn=findViewById<ImageView>(R.id.plusBtnDetails)
        val minusBtn=findViewById<ImageView>(R.id.minusBtnDetails)
        val addtocart=findViewById<AppCompatButton>(R.id.addTocartdetails)


       // getDetails(oid)
        plusBtn.setOnClickListener {
            numberOrder = numberOrder!! + 1
            numberOrderTxt!!.text = numberOrder.toString()
        }

        minusBtn.setOnClickListener {
            if (numberOrder!! > 1) {
                numberOrder = numberOrder!! - 1
            }
            numberOrderTxt!!.text = numberOrder.toString()
        }
        //cartModel = intent.getSerializableExtra("cartModel") as CartModel?
        //orderRef=FirebaseDatabase.getInstance().reference.child("Artisans").child(aid!!).child("Pattern")
        artisanRef=FirebaseDatabase.getInstance().reference.child("Artisans").child(aid!!)
        orderRef=FirebaseDatabase.getInstance().reference.child("Pattern")
        val imageList = ArrayList<SlideModel>()
        if(oid!=null){
            orderRef!!.child(oid!!).addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        var orderModel : OrderModel?=snapshot.getValue(OrderModel::class.java)
                        dimensions!!.text=orderModel!!.dimensions
                        price!!.text=orderModel.price
                        color!!.text=orderModel.color
                        description!!.text=orderModel.description

                       // Glide.with(this@ShowDetailsActivity).load(orderModel.image).into(image!!)

                        imagestring=orderModel.image.toString()


                        orderRef!!.child(oid!!).child("images").addListenerForSingleValueEvent(object:ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                imageList.add(SlideModel(orderModel.image))
                               for(datasnashot in snapshot.children){

                                   imageList.add(SlideModel(datasnashot.child("image").value.toString()))
                               }
                                imageSlider!!.setImageList(imageList,ScaleTypes.CENTER_CROP)
                                imageSlider!!.stopSliding()
                                //setting itemclicklistener on imageslider
//                                imageSlider!!.setItemClickListener(object : ItemClickListener {
//                                    override fun onItemSelected(position: Int) {
//
//                                    }
//                                })
                            }

                            override fun onCancelled(error: DatabaseError) {
                                TODO("Not yet implemented")
                            }

                        })

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }

        addtocart.setOnClickListener(View.OnClickListener {

            if(priority!!.isChecked){
                highPriority = "High"
            }
            else{
                highPriority = "Low"
            }

           SubmitToCart(numberOrder.toString(),imagestring, oid!!,highPriority!!)
//            cartModel!!.numberInCart=(numberOrder)
//            managementCart!!.insertFood(cartModel!!)
        })

        addImageButton!!.setOnClickListener(View.OnClickListener {

            val intent= Intent(this@ShowDetailsActivity, AddImage::class.java)
            intent.putExtra("oid",oid)
            startActivity(intent)

        })

    }

    private fun getDetails(oid: String?) {
//orderRef= FirebaseDatabase.getInstance().reference.child("Artisans").child(oid!!).child("Order")
orderRef=FirebaseDatabase.getInstance().reference.child("Artisans").child(aid!!).child("Pattern")
        if(oid!=null){
            orderRef!!.child(oid).addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        var orderModel : OrderModel?=snapshot.getValue(OrderModel::class.java)
                        dimensions!!.text=orderModel!!.dimensions
                        price!!.text=orderModel.price
                        color!!.text=orderModel.color
                        description!!.text=orderModel.description

                        Glide.with(this@ShowDetailsActivity).load(orderModel.image).into(image!!)

                        //SubmitImage(orderModel.image)

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            })
        }
    }


    private fun SubmitToCart(
        quantity: String,
        imagestring: String?,
        oid: String,
        highPriority: String
    ) {
orderRef!!.child(oid).addValueEventListener(object :ValueEventListener{
    override fun onDataChange(snapshot: DataSnapshot) {
       if(snapshot.exists()){
           var orderModel : OrderModel?=snapshot.getValue(OrderModel::class.java)
           var category: String?=orderModel!!.category


           cartRef=FirebaseDatabase.getInstance().reference.child("CartOrder")

           val calendar = Calendar.getInstance()

           val currentDate = SimpleDateFormat("MMM dd, yyyy")
           saveCurrentDate = currentDate.format(calendar.time)

           val currentTime = SimpleDateFormat("HH:mm:ss a")
           saveCurrentTime = currentTime.format(calendar.time)

           cartRandomKey = saveCurrentDate + saveCurrentTime

           // cartMap.put("image",productImage)
           val rating = ratingBar!!.rating.toString()
           val cartMap = HashMap<String, Any>()
           cartMap["oid"]=cartRandomKey!!
           cartMap["dimensions"]=dimensions!!.text.toString()
           cartMap["image"]=imagestring!!
           cartMap["color"]=color!!.text.toString()
           cartMap["description"]=description!!.text.toString()
           cartMap["quantity"]=quantity
           cartMap["price"]=price!!.text.toString()
           cartMap["rating"]=rating
           cartMap["category"]=category!!
           cartMap["priority"]= highPriority



           cartRef?.child(cartRandomKey!!)?.updateChildren(cartMap)
               ?.addOnCompleteListener(OnCompleteListener { task ->
                   if (task.isSuccessful) {
                       orderRef!!.child(oid).child("images")?.addValueEventListener(object:ValueEventListener{
                           override fun onDataChange(snapshot: DataSnapshot) {
                               for(datasnapshot in snapshot.children) {
                                   var imageModel: ImageModel? =
                                       datasnapshot.getValue(ImageModel::class.java)
                                   val imageMap = HashMap<String, Any>()
                                   imageMap["image"] = imageModel!!.image!!
                                   imageMap["iid"] = imageModel!!.iid!!
                                   cartRef?.child(cartRandomKey!!)?.child("images")
                                       ?.child(imageModel!!.iid!!)?.updateChildren(imageMap)
                                       ?.addOnCompleteListener(OnCompleteListener { task ->
                                           if (task.isSuccessful) {
                                               Log.d(
                                                   "ShowDetailsActivity",
                                                   "Successfully added images in cart ref"
                                               )
                                           } else {
                                               Log.d(
                                                   "ShowDetailsActivity",
                                                   "Not added images in cart ref"
                                               )
                                           }
                                       })
                               }
                           }

                           override fun onCancelled(error: DatabaseError) {
                               TODO("Not yet implemented")
                           }

                       })



//                       cartRef?.child(cartRandomKey!!)?.child("images").updateChildren()



                       val intent = Intent(this@ShowDetailsActivity, CartListActivity::class.java)
                       intent.putExtra("oid",cartRandomKey)
//                       val sharedPref = this@ShowDetailsActivity?.getPreferences(Context.MODE_PRIVATE) ?: return@OnCompleteListener
//                       with (sharedPref.edit()) {
//                           putString(getString(R.string.oid), oid)
//                           apply()
//                       }
                       val sharedPreferences: SharedPreferences=getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
                       val editor=sharedPreferences.edit()
                       editor.apply{
                            putString("oid",cartRandomKey)
                       }.apply()
                       startActivity(intent)

                       Toast.makeText(
                           this@ShowDetailsActivity,
                           "Added to cart successfully..",
                           Toast.LENGTH_SHORT
                       ).show()
                   } else {
                       //loadingBar!!.dismiss()
                       val message = task.exception.toString()
                       Toast.makeText(this@ShowDetailsActivity, "Error: $message", Toast.LENGTH_SHORT).show()
                   }
               })

       }
    }

    override fun onCancelled(error: DatabaseError) {

    }

})



    }


}