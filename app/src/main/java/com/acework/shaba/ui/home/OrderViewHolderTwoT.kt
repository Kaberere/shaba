package com.acework.shaba.ui.home

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R


class OrderViewHolderTwoT (itemView: View) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener {
    var txtOrderName: TextView
    var imageView: ImageView
    var txtDimensions: TextView
    var txtDate: TextView
    private var listener: ItemClickListener? = null
    fun setItemClickListener(listener: ItemClickListener?) {
        this.listener = listener
    }
    override fun onClick(v: View?) {
        listener?.onClick(v, adapterPosition, false)
    }
    init {
        imageView = itemView.findViewById<ImageView>(R.id.order_image)
        txtOrderName = itemView.findViewById<TextView>(R.id.order_name)
        txtDimensions=itemView.findViewById<TextView>(R.id.order_color)
        txtDate=itemView.findViewById<TextView>(R.id.order_date)
        itemView.setOnClickListener(this)
    }

}