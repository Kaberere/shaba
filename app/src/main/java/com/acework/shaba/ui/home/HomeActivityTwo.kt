package com.acework.shaba.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.acework.shaba.MainActivity
import com.acework.shaba.R
import com.acework.shaba.admin.orders.cart.CartModel
import com.acework.shaba.home.HomeActivity
import com.acework.shaba.ui.home.orders.Orders
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*

class HomeActivityTwo : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar? = null

    private var orderAdapter: OrderRecyclerAdapter?= null
    private var images: IntArray? =null;
    private var cartRef: DatabaseReference? = null
    private var orderRef: DatabaseReference?=null
    private var keysT: String?=null
    private var cid: String?=null
    private var category:String?=null
    private var completeorderRef: DatabaseReference?=null

    private var completeRandomKey: String? = null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        progressBar = findViewById<ProgressBar>(R.id.progress_bar_orders)


        val drawer = findViewById<DrawerLayout>(R.id.drawer_layouttwo);

        val navigationView = findViewById<NavigationView>(R.id.nav_view)

        val toolbar: Toolbar = findViewById<Toolbar>(R.id.toolbar_homeC)
        setSupportActionBar(toolbar)

        val floatingbar= findViewById<FloatingActionButton>(R.id.completeorderfab)

        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)

        recyclerView=findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager

        cid= intent!!.getStringExtra("cid")
        category=intent!!.getStringExtra("category")
        progressBar!!.visibility = View.GONE

        if(category.equals("Weaver")) {
            cartRef = FirebaseDatabase.getInstance().reference.child("CheckOut")
        }
        else if(category.equals("Wood work")){
            cartRef = FirebaseDatabase.getInstance().reference.child("CheckOutWood")
        }
//getData(cartRef!!)


        orderRef=cartRef!!.child(cid!!).child("Order")


        bindView(orderRef)


        val calendar = Calendar.getInstance()

        val currentDate = SimpleDateFormat("MMM dd, yyyy")
        saveCurrentDate = currentDate.format(calendar.time)

        val currentTime = SimpleDateFormat("HH:mm:ss a")
        saveCurrentTime = currentTime.format(calendar.time)

        completeRandomKey = saveCurrentDate + saveCurrentTime


        floatingbar.setOnClickListener(View.OnClickListener {
            if(category.equals("Weaver")) {
                completeorderRef=FirebaseDatabase.getInstance().reference.child("CompleteOrder")
                moveRecord(cartRef!!.child(cid!!),completeorderRef!!.child(cid!!))
            }
            else if(category.equals("Wood work")){
                completeorderRef=FirebaseDatabase.getInstance().reference.child("CompleteOrderWood")
                moveRecord(cartRef!!.child(cid!!),completeorderRef!!.child(cid!!))
            }


            val intent=Intent(this,HomeActivity::class.java)
            startActivity(intent)
            finish()

            cartRef!!.child(cid!!).removeValue()
        })

    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
            val id: Int = item.getItemId()

            if (id == R.id.nav_home_user) {
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
            }
            else if(id == R.id.nav_payment_user){

                val intent = Intent(this, HomeActivity::class.java)

                startActivity(intent)
            }
            else if(id==R.id.nav_logout_user){
                AuthUI.getInstance().signOut(this).addOnCompleteListener(OnCompleteListener {
                    val intent = Intent(this, MainActivity::class.java)
                    FirebaseAuth.getInstance().signOut();
                    startActivity(intent)
                })

            }
            return true
    }
    private fun bindView(orderRef: DatabaseReference?) {


        var options = FirebaseRecyclerOptions.Builder<CartModel>()
            .setQuery(orderRef!!, CartModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT> = object :
            FirebaseRecyclerAdapter<CartModel, OrderViewHolderTwoT>(options) {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): OrderViewHolderTwoT {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.orders_layout, parent, false)
                return OrderViewHolderTwoT(view)
            }

            override fun onBindViewHolder(
                orderViewHolderTwoT: OrderViewHolderTwoT,
                p1: Int,
                cartModel: CartModel
            ) {
                val oid = cartModel.oid

                orderViewHolderTwoT.txtOrderName.text = cartModel.color
                orderViewHolderTwoT.txtDimensions.text = cartModel.dimensions
                orderViewHolderTwoT.txtDate.text = cartModel.oid

                Glide.with(this@HomeActivityTwo).load(cartModel.image)
                    .into(orderViewHolderTwoT.imageView)

                orderViewHolderTwoT.itemView.setOnClickListener(View.OnClickListener {
                    val intent = Intent(this@HomeActivityTwo, Orders::class.java)
                    intent.putExtra("oid", oid)
                    intent.putExtra("key",cid)
                    intent.putExtra("category",category)
                    startActivity(intent)
                })
            }

        }

        recyclerView!!.adapter = adapter
        adapter.startListening()
    }

    private fun moveRecord(fromPath: DatabaseReference, toPath: DatabaseReference)
    {
        val valueEventListener: ValueEventListener = object : ValueEventListener
        {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                toPath.setValue(dataSnapshot.value).addOnCompleteListener { task ->
                    if (task.isComplete) {
                        Log.d("TAG", "Success!")
                        Toast.makeText(
                            this@HomeActivityTwo,
                            "Completed task successfully..",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Log.d("TAG", "Copy failed!")
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        }
        fromPath.addListenerForSingleValueEvent(valueEventListener)
        }
}