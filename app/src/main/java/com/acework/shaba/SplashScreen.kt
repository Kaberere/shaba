package com.acework.shaba

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SplashScreen : AppCompatActivity() {
    private val SPLASH_SCREEN = 2000

    //Variables
    var topAnim: Animation? = null
    var bottomAnim: Animation? = null
    var image: ImageView?= null
    var logo: TextView? = null
    var slogan: TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        //Animation

        //Animation
        topAnim = AnimationUtils.loadAnimation(this, R.anim.topanimation)
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottomanimation)


          image=findViewById<ImageView>(R.id.imageView)
        logo = findViewById<TextView>(R.id.textView)



        //  image.setAnimation(topAnim);

         image!!.animation=topAnim;
       // logo!!.animation=(bottomAnim)
       // slogan.setAnimation(bottomAnim)

        Handler().postDelayed({
            val intent = Intent(this@SplashScreen, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, SPLASH_SCREEN.toLong())


    }
}