package com.acework.shaba.admin.completeorder.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.acework.shaba.R
import com.acework.shaba.admin.completeorder.fragment.singleorders.SingleOrders
import com.acework.shaba.admin.orders.cart.CheckOutModel
import com.acework.shaba.ui.home.organize.CategoriesViewHolder
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class CompleteWeaverFragment : Fragment()  {
    private var completeRef: DatabaseReference? = null
   // private var checkoutRefWood: DatabaseReference? = null
    private var recyclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var progressBar: ProgressBar?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_home, container, false)
        completeRef= FirebaseDatabase.getInstance().reference.child("CompleteOrder")
        progressBar=view.findViewById(R.id.progress_bar_orders)
        recyclerView=view.findViewById(R.id.orders_list)
        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(activity)
        recyclerView!!.layoutManager = layoutManager
        val textgone: TextView =view.findViewById(R.id.textgone)

        completeRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    bindView(completeRef!!)
                    textgone.visibility=View.INVISIBLE
                }
                else{
                    Toast.makeText(activity,"Currently no completed orders available", Toast.LENGTH_LONG).show()
                    progressBar!!.visibility = ProgressBar.INVISIBLE
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

        return view
    }

    private fun bindView(completeRef: DatabaseReference) {
        var options = FirebaseRecyclerOptions.Builder<CheckOutModel>()
            .setQuery(completeRef, CheckOutModel::class.java)
            .build()

        var adapter: FirebaseRecyclerAdapter<CheckOutModel,CategoriesViewHolder> = object :
        FirebaseRecyclerAdapter<CheckOutModel,CategoriesViewHolder>(options){
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): CategoriesViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.categories_layout, parent, false)
                return CategoriesViewHolder(view)
            }

            override fun onBindViewHolder(categoriesViewHolder: CategoriesViewHolder, p1: Int, checkOutModel: CheckOutModel) {
                var cid=checkOutModel.cid
                var total=checkOutModel.total

                categoriesViewHolder.txtDate.text=checkOutModel.cid
                categoriesViewHolder.txtTotal.text=total

                progressBar!!.visibility = ProgressBar.INVISIBLE

                categoriesViewHolder.itemView.setOnClickListener(View.OnClickListener {
                    val intent= Intent(activity, SingleOrders::class.java)
                    intent.putExtra("cid",cid)
                   intent.putExtra("category","Weaver")
                    startActivity(intent)
                })
            }

        }
        recyclerView!!.adapter = adapter
        adapter.startListening()
    }
}