package com.acework.shaba

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.acework.shaba.home.HomeActivity
import com.acework.shaba.register.PhoneOTP
import com.acework.shaba.register.Register
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase

class LoginTabFragment: Fragment() {
private var email: EditText?=null
    private var password: EditText?=null
    private var forgot: TextView?=null
    private var loadingBar: ProgressDialog? = null
    private lateinit var auth: FirebaseAuth
    private var userRef: DatabaseReference? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.login_tab_fragment, container, false)

        email=view.findViewById<EditText>(R.id.email)
        //password=view.findViewById<EditText>(R.id.pass)
        //forgot=view.findViewById<TextView>(R.id.forgotPass)
        var register=view.findViewById<TextView>(R.id.signup)

        val login=view.findViewById<AppCompatButton>(R.id.login)
        loadingBar = ProgressDialog(activity)


        email!!.translationX=800.toFloat()
//        password!!.translationX=800.toFloat()
//        forgot!!.translationX=800.toFloat()
        login.translationX=800.toFloat()

        email!!.alpha=0.toFloat()
//        password!!.alpha=0.toFloat()
//        forgot!!.alpha=0.toFloat()
        login.alpha=0.toFloat()


        email!!.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(300).start()
//        password!!.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(500).start()
//        forgot!!.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(500).start()
        login.animate().translationX(0.toFloat()).alpha(1.toFloat()).setDuration(800).setStartDelay(700).start()
        // ...
// Initialize Firebase Auth
        auth = Firebase.auth
        login.setOnClickListener {
//            val intent= Intent(activity, HomeActivity::class.java )
//            startActivity(intent)
            LoginUser()
        }

        userRef= FirebaseDatabase.getInstance().reference.child("Users")

        register.setOnClickListener(View.OnClickListener {
            val intent= Intent(activity, Register::class.java )
            startActivity(intent)
        })



        return view
    }

    //For accounts to remember users login credentials

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser != null){
            val intent= Intent(activity, HomeActivity::class.java )
            intent.putExtra("user",currentUser)
            startActivity(intent)
        }
    }
    private fun LoginUser() {
        val email=email!!.text.toString()
            //  val password=password!!.text.toString()
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(activity, "Please write your phonenumber...", Toast.LENGTH_SHORT).show()
        }
//        else if (TextUtils.isEmpty(password)) {
//            Toast.makeText(activity, "Please write your password...", Toast.LENGTH_SHORT).show()
//        }
        else {
            loadingBar!!.setTitle("Login Account")
            loadingBar!!.setMessage("Please wait, while we are checking the credentials.")
            loadingBar!!.setCanceledOnTouchOutside(false)
            loadingBar!!.show()
            AllowAccessToAccount(email)
        }
    }

    private fun AllowAccessToAccount(email: String) {
        activity?.let {
//            auth.signInWithEmailAndPassword(email, password)
//                .addOnCompleteListener(it) { task ->
//                    if (task.isSuccessful) {
//                        // Sign in success, update UI with the signed-in user's information
//                        Log.d("TAG", "signInWithEmail:success")
//                        val user = auth.currentUser
//                        val intent= Intent(activity, HomeActivity::class.java )
//                        intent.putExtra("user",user)
//                        startActivity(intent)
//
//                    } else {
//                        // If sign in fails, display a message to the user.
//                        Log.w("TAG", "signInWithEmail:failure", task.exception)
//                        Toast.makeText(activity, "Authentication failed.",
//                            Toast.LENGTH_SHORT).show()
//                      //  updateUI(null)
//                    }
//                }

            userRef!!.addListenerForSingleValueEvent (object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {

                    if(snapshot.child(email).exists()){
                        val intent = Intent(activity, PhoneOTP::class.java)
                        intent.putExtra("phoneNo",email)
                        startActivity(intent)
                    }
                    else{
                        Toast.makeText(activity, "Kindly register first inorder to login",
                           Toast.LENGTH_SHORT).show()
                        loadingBar!!.dismiss()
                    }

                }

                override fun onCancelled(error: DatabaseError) {

                }

            })




        }
    }
}