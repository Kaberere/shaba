package com.acework.shaba.admin.orders.cart

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.acework.shaba.R
import com.acework.shaba.admin.AdminHome
import com.acework.shaba.admin.orders.cart.`interface`.ChangeNumberItemsListener
import com.acework.shaba.admin.orders.cart.recyclerview.CartAdapter
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class CartListActivity : AppCompatActivity() {
    private var image: ImageView?=null
    private var color: TextView?= null
    private var fee: TextView?=null
    private var totalfee: TextView?=null
    private var total: TextView?= null
    private var cartRef: DatabaseReference? = null
    private var checkoutRef: DatabaseReference? = null
    private var checkoutRefWood: DatabaseReference? = null
    private var checkoutRefWeaver: DatabaseReference? = null
    private var checkoutRefWoodwork: DatabaseReference? = null
    private var recyclerView: RecyclerView? = null
    private var numberOrder: Int?=1
    private var checkOut: AppCompatButton?=null
    private var completeRandomKey: String? = null
    private var saveCurrentDate: String?=null
    private var saveCurrentTime: String?=null
private var totalprice: Int=0;
    private var totals: Int=0;
    private var oid: String?=null
    private lateinit var cartList: ArrayList<CartModel>
    private lateinit var cartAdapter: CartAdapter
    private var dataInCart: Int? = null
    private var changeNumberItemsListener: ChangeNumberItemsListener= ChangeNumberItemsListener {  }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart_list)


        image=findViewById(R.id.cart_image)
        color=findViewById(R.id.cart_color)
        totalfee=findViewById(R.id.totalFeeTxt)
        total=findViewById(R.id.totalTxt)
checkOut=findViewById(R.id.completeOrder)

        cartRef= FirebaseDatabase.getInstance().reference.child("CartOrder")
        checkoutRef= FirebaseDatabase.getInstance().reference.child("CheckOut")
        checkoutRefWood= FirebaseDatabase.getInstance().reference.child("CheckOutWood")
        checkoutRefWeaver=FirebaseDatabase.getInstance().reference.child("CheckOutWeaver")
        checkoutRefWoodwork=FirebaseDatabase.getInstance().reference.child("CheckOutWoodWork")

        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView=findViewById(R.id.cart_list)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = linearLayoutManager
        oid = intent.getStringExtra("oid")

        val sharedPreferences: SharedPreferences =getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val oidtwo: String?=sharedPreferences.getString("oid",null)
        val calendar = Calendar.getInstance()

        val currentDate = SimpleDateFormat("MMM dd, yyyy")
        saveCurrentDate = currentDate.format(calendar.time)

        val currentTime = SimpleDateFormat("HH:mm:ss a")
        saveCurrentTime = currentTime.format(calendar.time)

        completeRandomKey = saveCurrentDate + saveCurrentTime

       // productsInCart(completeRandomKey!!)
        totalprice=0



        cartList = ArrayList()
        cartAdapter = CartAdapter(cartList,this, changeNumberItemsListener)

        addInfo()

        checkOut!!.setOnClickListener(View.OnClickListener {

//            cartRef!!.addListenerForSingleValueEvent(object : ValueEventListener{
//                override fun onDataChange(snapshot: DataSnapshot) {
//                    Log.d("Snapshot value", snapshot.value.toString())
//                    var value: String?=snapshot.value.toString()
//                    cartRef!!.child(value!!).addListenerForSingleValueEvent(object:ValueEventListener{
//                        override fun onDataChange(snapshot: DataSnapshot) {
//                            var cartModel:CartModel?=snapshot.getValue(CartModel::class.java)
//                            if((cartModel!!.category).equals("Weaver")){
//                                val completeMap=HashMap<String, Any>()
//
//                                completeMap["total"]=totalprice.toString()
//                                completeMap["cid"]=completeRandomKey!!
//                                checkoutRef?.child(completeRandomKey!!)?.updateChildren(completeMap)
//                                    ?.addOnCompleteListener(OnCompleteListener { task ->
//                                        if (task.isSuccessful) {
//                                            Toast.makeText(
//                                                this@CartListActivity,
//                                                "Checkout began..",
//                                                Toast.LENGTH_SHORT
//                                            ).show()
//
//                                            val checkoutRefTwo= FirebaseDatabase.getInstance().reference.child("CheckOut").child(completeRandomKey!!).child("Order")
//                                            moveRecord(cartRef!!,checkoutRefTwo)
//
//                                            val intent = Intent(this@CartListActivity, AdminHome::class.java)
//                                            startActivity(intent)
//
//                                            cartRef!!.removeValue()
//                                        }
//                                        else {
//                                            val message = task.exception.toString()
//                                            Toast.makeText(this@CartListActivity, "Error: $message", Toast.LENGTH_SHORT).show()
//
//                                        }
//
//
//                                    })
//                            }
//                            else{
//                                val completeMap=HashMap<String, Any>()
//
//                                completeMap["total"]=totalprice.toString()
//                                completeMap["cid"]=completeRandomKey!!
//
//                                checkoutRefWood?.child(completeRandomKey!!)?.updateChildren(completeMap)
//                                    ?.addOnCompleteListener(OnCompleteListener { task ->
//                                        if (task.isSuccessful) {
//                                            Toast.makeText(
//                                                this@CartListActivity,
//                                                "Checkout began..",
//                                                Toast.LENGTH_SHORT
//                                            ).show()
//
//                                            val checkoutRefTwo= FirebaseDatabase.getInstance().reference.child("CheckOutWood").child(completeRandomKey!!).child("Order")
//                                            moveRecord(cartRef!!,checkoutRefTwo)
//
//                                            val intent = Intent(this@CartListActivity, AdminHome::class.java)
//                                            startActivity(intent)
//
//                                            cartRef!!.removeValue()
//                                        }
//                                        else {
//                                            val message = task.exception.toString()
//                                            Toast.makeText(this@CartListActivity, "Error: $message", Toast.LENGTH_SHORT).show()
//
//                                        }
//
//
//                                    })
//                            }
//                        }
//
//                        override fun onCancelled(error: DatabaseError) {
//                            TODO("Not yet implemented")
//                        }
//
//                    })
//
//                }
//
//                override fun onCancelled(error: DatabaseError) {
//
//                }
//
//            })

            cartRef!!.addListenerForSingleValueEvent(object :ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    for(dataSnapshot in snapshot.children) {
                        cartRef!!.child(dataSnapshot.key!!).addListenerForSingleValueEvent(object: ValueEventListener{
                            override fun onDataChange(snapshots: DataSnapshot) {

                                if (snapshots!= null) {
                                    var cartModel: CartModel? = snapshots.getValue(CartModel::class.java)
                                    var category: String? = cartModel!!.category
                                    if (category!!.equals("Woodwork")) {

                                        val completeMap = HashMap<String, Any>()

                                        completeMap["total"] = totalprice.toString()
                                        completeMap["cid"] = completeRandomKey!!

                                        checkoutRefWood?.child(completeRandomKey!!)
                                            ?.updateChildren(completeMap)
                                            ?.addOnCompleteListener(OnCompleteListener { task ->
                                                if (task.isSuccessful) {
                                                    Toast.makeText(
                                                        this@CartListActivity,
                                                        "Checkout began..",
                                                        Toast.LENGTH_SHORT
                                                    ).show()

                                                    val checkoutRefTwo =
                                                        FirebaseDatabase.getInstance().reference.child("CheckOutWood")
                                                            .child(completeRandomKey!!).child("Order")
                                                    //Try to figure out how to transfer data without deleting after sending data

                                                    val newMap = HashMap<String, Any>()
                                                    newMap["category"] = cartModel!!.category!!
                                                    newMap["color"] = cartModel!!.color!!
                                                    newMap["description"] = cartModel!!.description!!
                                                    newMap["image"] = cartModel!!.image!!
                                                    newMap["oid"] = completeRandomKey!!
                                                    newMap["price"] = cartModel!!.price!!
                                                    newMap["quantity"] = cartModel!!.quantity!!
                                                    newMap["rating"] = cartModel!!.rating!!

//

//                                       checkoutRefWoodwork?.child(completeRandomKey!!)?.updateChildren(newMap)
//                                           ?.addOnCompleteListener(OnCompleteListener { task ->
//                                               if(task.isSuccessful){
//                                                   Log.d("CartlistActivity","Successful")
//                                               }
//                                               else{
//                                                   Log.d("CartlistActivity","Failed")
//                                               }
//                                           })
                                                    moveRecord(cartRef!!, checkoutRefWoodwork!!,dataSnapshot.key!!)
                                                    moveRecord(cartRef!!, checkoutRefTwo, dataSnapshot.key!!)


                                                    val intent =
                                                        Intent(this@CartListActivity, AdminHome::class.java)
                                                    startActivity(intent)
                                                    finish()
                                                   // cartRef!!.removeValue()
                                                } else {
                                                    val message = task.exception.toString()
                                                    Toast.makeText(
                                                        this@CartListActivity,
                                                        "Error: $message",
                                                        Toast.LENGTH_SHORT
                                                    ).show()

                                                }


                                            })


                                    } else if (category!!.equals("Weaver")) {
                                        val completeMap = HashMap<String, Any>()

                                        completeMap["total"] = totalprice.toString()
                                        completeMap["cid"] = completeRandomKey!!
                                        checkoutRef?.child(completeRandomKey!!)?.updateChildren(completeMap)
                                            ?.addOnCompleteListener(OnCompleteListener { task ->
                                                if (task.isSuccessful) {
                                                    Toast.makeText(
                                                        this@CartListActivity,
                                                        "Checkout began..",
                                                        Toast.LENGTH_SHORT
                                                    ).show()


//                                       checkoutRef?.addListenerForSingleValueEvent(object : ValueEventListener{
//                                           override fun onDataChange(snapshot: DataSnapshot) {
//                                                for(orders in snapshot.children){
//                                                   for(specificOrder in orders.child("Order").children ){
//
//                                                     var cartModel: CartModel?=specificOrder.getValue(CartModel::class.java)
//                                                       val orderMap=HashMap<String,CartModel>()
//                                                       orderMap[cartModel!!.oid!!]=cartModel!!
//
//                                                       //Continue from here
////                                                       val newMap=HashMap<String, Any>()
////                                                       newMap["category"]=cartModel!!.category!!
////                                                       newMap["color"]=cartModel!!.color!!
////                                                       newMap["description"]=cartModel!!.description!!
////                                                       newMap["image"]=cartModel!!.image!!
////                                                       newMap["oid"]=completeRandomKey!!
////                                                       newMap["price"]=cartModel!!.price!!
////                                                       newMap["quantity"]=cartModel!!.quantity!!
////                                                       newMap["rating"]=cartModel!!.rating!!
////
////                                                       checkoutRefWeaver?.child(completeRandomKey!!)?.updateChildren(orderMap)
////                                          ?.addOnCompleteListener(OnCompleteListener { task ->
////                                              if(task.isSuccessful){
////                                                   Log.d("CartlistActivity","Successful")
////                                             }
////                                           else{
////                                                 Log.d("CartlistActivity","Failed")
////                                              }
////                                          })
//
//                                                     //  moveRecord(specificOrder.ref,checkoutRefWeaver!!)
//                                                       //moveRecord(cartRef!!,checkoutRefWeaver!!)
//
//
//
//
//                                                   }
//                                               }
//                                           }
//
//                                           override fun onCancelled(error: DatabaseError) {
//
//                                           }
//
//
//                                       })

                                                    val checkoutRefTwo =
                                                        FirebaseDatabase.getInstance().reference.child("CheckOut")
                                                            .child(completeRandomKey!!).child("Order")
//                                       val newMap=HashMap<String, Any>()
//                                       newMap["category"]=cartModel!!.category!!
//                                       newMap["color"]=cartModel!!.color!!
//                                       newMap["description"]=cartModel!!.description!!
//                                       newMap["image"]=cartModel!!.image!!
//                                       newMap["oid"]=completeRandomKey!!
//                                       newMap["price"]=cartModel!!.price!!
//                                       newMap["quantity"]=cartModel!!.quantity!!
//                                       newMap["rating"]=cartModel!!.rating!!
//
//                                       checkoutRefWeaver?.child(completeRandomKey!!)?.updateChildren(newMap)
//                                           ?.addOnCompleteListener(OnCompleteListener { task ->
//                                               if(task.isSuccessful){
//                                                   Log.d("CartlistActivity","Successful")
//                                               }
//                                               else{
//                                                   Log.d("CartlistActivity","Failed")
//                                               }
//                                           })
                                                    moveRecord(cartRef!!, checkoutRefWeaver!!, dataSnapshot.key!!)
                                                    moveRecord(cartRef!!, checkoutRefTwo, dataSnapshot.key!!)


                                                    val intent =
                                                        Intent(this@CartListActivity, AdminHome::class.java)
                                                    startActivity(intent)

                                                    finish()

                                                   // cartRef!!.removeValue()
                                                } else {
                                                    val message = task.exception.toString()
                                                    Toast.makeText(
                                                        this@CartListActivity,
                                                        "Error: $message",
                                                        Toast.LENGTH_SHORT
                                                    ).show()

                                                }


                                            })

                                    }
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }

                        })

                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })


            //SubmitOrder(completeRandomKey!!,cartRef!!)

        })



    }

    private fun productsInCart(completeRandomKey:String) {
        var options= FirebaseRecyclerOptions.Builder<CartModel>()
            .setQuery(cartRef!!, CartModel::class.java)
            .build()



        var adapter: FirebaseRecyclerAdapter<CartModel, CartViewHolder> = object :
            FirebaseRecyclerAdapter<CartModel, CartViewHolder>(options){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_card, parent, false)

                return CartViewHolder(view)
            }

            override fun onBindViewHolder(cartViewHolder: CartViewHolder, position: Int, cartModel: CartModel) {
                val oid = cartModel.oid
                val itemClickListener: ChangeNumberItemsListener

                cartViewHolder.color.text=cartModel.color
                cartViewHolder.price.text=cartModel.price
                cartViewHolder.quantitynum.text=cartModel.quantity

                Glide.with(this@CartListActivity).load(cartModel.image).into(cartViewHolder.image)
                var feeOne: Int=Integer.parseInt(cartModel.price)
                var quantityone: Int=Integer.parseInt(cartViewHolder.quantitynum.text .toString())

                totals=quantityone * feeOne
                cartViewHolder.total.text=totals.toString()


                cartViewHolder.plusBtn.setOnClickListener(View.OnClickListener {
var totalsEach: Int=Integer.parseInt(cartViewHolder.total.text.toString())

                    numberOrder=Integer.parseInt(cartViewHolder.quantitynum.text .toString())
                        numberOrder = numberOrder!! + 1
                        cartViewHolder.quantitynum.text = numberOrder.toString()
                    NewPost(cartViewHolder.quantitynum.text .toString(),oid)

                    var feeOne: Int=Integer.parseInt(cartModel.price)
                    var quantityone: Int=Integer.parseInt(cartViewHolder.quantitynum.text .toString())

                    totals=(quantityone * feeOne)
                    totalprice=totalprice-totalsEach


                })

                cartViewHolder.minusBtn.setOnClickListener (View.OnClickListener {

                    numberOrder=Integer.parseInt(cartViewHolder.quantitynum.text .toString())

                    if(numberOrder!! >=2){
                        var totalsEach: Int=Integer.parseInt(cartViewHolder.total.text.toString())

                        numberOrder = numberOrder!! - 1
                        cartViewHolder.quantitynum.text = numberOrder.toString()

                        NewPost(cartViewHolder.quantitynum.text .toString(),oid)

                        var feeOne: Int=Integer.parseInt(cartModel.price)
                        var quantityone: Int=Integer.parseInt(cartViewHolder.quantitynum.text .toString())

                        totals=(quantityone * feeOne)
                        totalprice=totalprice-totalsEach


                    }
                    else if(numberOrder == 1) {
                        var totalsEach: Int=Integer.parseInt(cartViewHolder.price.text.toString())
                        totals=0
                        totalprice=totalprice-totalsEach

                        totalfee!!.text=totalprice.toString()
                        total!!.text=totalprice.toString()


                        cartRef!!.child(oid!!).removeValue().addOnCompleteListener(
                            OnCompleteListener { task ->
                                if(task.isSuccessful)
                                {
                                    Toast.makeText(
                                        this@CartListActivity,
                                        "Item removed successfully",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            })


                    }
                })


                totalprice += totals
                totalfee!!.text=totalprice.toString()
                total!!.text=totalprice.toString()




//
//                cartViewHolder.minusBtn.setOnClickListener (View.OnClickListener{
//                    if (numberOrder!! > 1) {
//                        numberOrder = numberOrder!! - 1
//                    }
//                    else if(numberOrder!!<1){
//
//                        cartRef!!.child(oid!!).removeValue().addOnCompleteListener(
//                            OnCompleteListener { task ->
//                             if(task.isSuccessful)
//                             {
//                                 Toast.makeText(
//                                     this@CartListActivity,
//                                     "Item removed successfully",
//                                     Toast.LENGTH_SHORT
//                                 ).show()
//                             }
//                            })
//
//
//                    }
//                    cartViewHolder.quantitynum.text = numberOrder.toString()
//                 //   Total(numberOrder!!,cartModel.price,cartViewHolder.total.text)
//                })



//                Toast.makeText(
//                    this@CartListActivity,
//                    "CardViewHolder" +(numberOrder).toString(),
//                    Toast.LENGTH_SHORT
//                ).show()


//                var feeOne: Int=Integer.parseInt(cartModel.price)
//                var quantityone: Int=Integer.parseInt(cartViewHolder.quantitynum.text .toString())
//
//
//
//                totals=quantityone * feeOne
//
//                cartViewHolder.total.text=totals.toString()



                //CheckOut(totalprice,completeRandomKey)
            }


        }

        recyclerView!!.adapter = adapter
        adapter.startListening()


    }



    private fun NewPost(quantity: String, oid: String?) {

        cartRef!!.child(oid!!).child("quantity").setValue(quantity).addOnSuccessListener {
Log.d("CartListActivity","Successful quantitiy update")
        }
            .addOnFailureListener{
                Log.d("CartListActivity","Failure quantitiy update")
            }

    }


    private fun moveRecord(fromPath: DatabaseReference, toPath: DatabaseReference, oidtwo: String) {
//        val valueEventListener: ValueEventListener = object : ValueEventListener {
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//
////                var updates=HashMap<String, Any>()
////
////                //this is not working
////                var rootRef=FirebaseDatabase.getInstance().reference
////
////                var checkoutweaver=concat("CheckOutWeaver",)
////                updates["/CheckOutWeaver"]=dataSnapshot.value as Object
////                    updates["/CartOrder"] = null as Object
////
////
////
////                rootRef.updateChildren(updates!!).addOnSuccessListener(object :OnSuccessListener<Void> {
////                    override fun onSuccess(p0: Void?) {
////                        Log.d("REMOVED", fromPath.key!!)
////                    }
////
////
////                })
////
//
//
////                toPath.setValue(dataSnapshot.value).addOnCompleteListener { task ->
////                    if (task.isComplete) {
//////                        var updates=HashMap<String, Any>()
//////                        updates[toPath.toString()]= dataSnapshot.value as Any
//////                        //updates[fromPath.toString()]=null as Any
//////                        var rootRef=FirebaseDatabase.getInstance().reference
//////
//////                        rootRef.updateChildren(updates).addOnSuccessListener {
//////                            Log.d("REMOVED", fromPath.key!!) }
////                        Log.d("TAG", "Success!")
////
////                        Toast.makeText(
////                            this@CartListActivity,
////                            "Order submitted successfully..",
////                            Toast.LENGTH_SHORT
////                        ).show()
////                        fromPath.child()
////                    } else {
////                        Log.d("TAG", "Copy failed!")
////                    }
////                }
//
//
////fromPath.addListenerForSingleValueEvent(object : ValueEventListener{
////    override fun onDataChange(snapshot: DataSnapshot) {
////        toPath.setValue(dataSnapshot.value)
////    }
////
////    override fun onCancelled(error: DatabaseError) {
////        TODO("Not yet implemented")
////    }
////
////})
//
//            }
//
//            override fun onCancelled(databaseError: DatabaseError) {}
//        }
//        //fromPath.addListenerForSingleValueEvent(valueEventListener)



                   fromPath.child(oidtwo!!).addListenerForSingleValueEvent(object:ValueEventListener{
                       override fun onDataChange(snapshot: DataSnapshot) {
                           //    for (datasnapshot in snapshot.children) {
                           toPath.child(snapshot.key!!)
                               .setValue(snapshot.value, object : DatabaseReference.CompletionListener {
                                   override fun onComplete(error: DatabaseError?, ref: DatabaseReference) {
                                       if (error == null) {
                                           Log.i("CartListActivity", "onComplete: success");
                                           // In order to complete the move, we are going to erase
                                           // the original copy by assigning null as its value.
                                           fromPath.child(oidtwo).setValue(null);
                                       } else {
                                           Log.e(
                                               "CartListActivity",
                                               "onComplete: failure:" + error.message + ": "
                                                       + error.details
                                           );
                                       }
                                   }

                               })
                           //  }
                       }

                       override fun onCancelled(error: DatabaseError) {
                           Log.e("CartListActivity", "onComplete: failure:" + error.message + ": "
                                   + error.details
                           );
                       }

                   })


    }
private fun addInfo(){

    cartRef!!.addValueEventListener(object : ValueEventListener{
        override fun onDataChange(snapshot: DataSnapshot) {
            cartList.clear() // to avoid duplication in the recyclerview when items are updated
           for(dataInCart in snapshot.children){

               var cartModel:CartModel?=dataInCart.getValue(CartModel::class.java)
               cartList.add(cartModel!!)

                   getTotalFee()
                   totalfee!!.text = totalprice.toString()
                   total!!.text = totalprice.toString()
               cartAdapter.notifyDataSetChanged()
           }
            cartAdapter = CartAdapter(cartList,this@CartListActivity,changeNumberItemsListener)
            recyclerView!!.adapter = cartAdapter

        }

        override fun onCancelled(error: DatabaseError) {
            TODO("Not yet implemented")
        }

    })

}


    private fun  getTotalFee(): Int?{
totalprice=0
        for(totalProducts in 0 until cartList.size){
            totalprice=totalprice + (Integer.parseInt(cartList.get(totalProducts).price) * Integer.parseInt(cartList.get(totalProducts).quantity))
        }
        return totalprice


    }
}

